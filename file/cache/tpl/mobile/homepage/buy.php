<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header', $template);?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
</div>
<div class="head-bar-fix"></div>
</div>
<?php if($itemid) { ?>
<div class="main">
<div class="title"><strong><?php echo $title;?></strong></div>
<div class="info"><?php echo $editdate;?><?php if($MOD['hits']) { ?>&nbsp;&nbsp;浏览:<span id="hits"><?php echo $hits;?></span><?php } ?>
</div>
<?php if($thumb) { ?><?php include template('album', 'chip');?><?php } ?>
<div class="content">
<?php if(!$username) { ?><span style="color:red;">非会员信息</span><br/><?php } ?>
<?php if($vip) { ?><?php echo VIP;?>:<?php echo $vip;?>级<br/><?php } ?>
<?php if($n1 && $v1) { ?><?php echo $n1;?>:<?php echo $v1;?><br/><?php } ?>
<?php if($n2 && $v2) { ?><?php echo $n2;?>:<?php echo $v2;?><br/><?php } ?>
<?php if($n3 && $v3) { ?><?php echo $n3;?>:<?php echo $v3;?><br/><?php } ?>
<?php if($amount) { ?>数量:<?php echo $amount;?><br/><?php } ?>
<?php if($price) { ?>价格:<?php echo $price;?><br/><?php } ?>
<?php if($pack) { ?>包装:<?php echo $pack;?><br/><?php } ?>
<?php if($could_price) { ?>
<a href="<?php echo $MOD['mobile'];?><?php echo rewrite('price.php?itemid='.$itemid);?>" data-transition="slideup"><div class="btn-blue" style="margin:10px 0;">发送报价</div></a>
<?php } ?>
<?php echo $content;?>
</div>
<div class="head bd-b"><strong>联系方式</strong></div>
<div class="contact"><?php include template('contact', 'chip');?></div>
</div>
<?php } else { ?>
<?php if($lists) { ?>
<?php if(is_array($lists)) { foreach($lists as $v) { ?>
<div class="list-img">
<a href="<?php echo $v['linkurl'];?>"><img src="<?php if($v['thumb']) { ?><?php echo $v['thumb'];?><?php } else { ?><?php echo DT_MOB;?>static/img/60x60.png<?php } ?>
" width="60" height="60" alt="" onerror="this.src='<?php echo DT_MOB;?>static/img/60x60.png';"/></a>
<ul>
<li><a href="<?php echo $v['linkurl'];?>"><strong><?php echo $v['title'];?></strong></a></li>
<li><em class="f_r"><?php echo timetodate($v['edittime'], 5);?></em><?php if($MOD['hits']) { ?><span>浏览:<?php echo $v['hits'];?></span><?php } ?>
</li>
</ul>
</div>
<?php } } ?>
<?php } else { ?>
<?php include template('empty', 'chip');?>
<?php } ?>
<?php if($pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>
<?php } ?>
<?php include template('footer', $template);?>