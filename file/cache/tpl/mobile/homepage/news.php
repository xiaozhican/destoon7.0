<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header', $template);?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>" data-direction="reverse"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
<div class="head-bar-right">
<?php if($TYPE) { ?>
<a href="javascript:window.scrollTo(0,0);$('#type-<?php echo $file;?>').slideToggle(300);"><img src="<?php echo DT_MOB;?>static/img/icon-action.png" width="24" height="24"/></a>
<?php } ?>
</div>
</div>
<div class="head-bar-fix"></div>
</div>
<?php if($TYPE) { ?>
<div id="type-<?php echo $file;?>" style="display:none;">
<div class="list-set">
<ul>
<?php if(is_array($_TP['0'])) { foreach($_TP['0'] as $v0) { ?>
<li<?php if($typeid==$v0['typeid']) { ?> style="background:#F6F6F6;"<?php } ?>
><div><a href="<?php echo userurl($username, 'file='.$file.'&typeid='.$v0['typeid'], $domain);?>"><?php echo $v0['typename'];?></a></div></li>
<?php if(isset($_TP['1'][$v0['typeid']])) { ?>
<?php if(is_array($_TP['1'][$v0['typeid']])) { foreach($_TP['1'][$v0['typeid']] as $v1) { ?>
<li<?php if($typeid==$v1['typeid']) { ?> style="background:#F6F6F6;"<?php } ?>
><div><a href="<?php echo userurl($username, 'file='.$file.'&typeid='.$v1['typeid'], $domain);?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $v1['typename'];?></a></div></li>
<?php } } ?>
<?php } ?>
<?php } } ?>
</ul>
</div>
<div class="blank-35 bd-b"></div>
</div>
<?php } ?>
<?php if($itemid) { ?>
<div class="main">
<div class="title"><strong><?php echo $title;?></strong></div>
<div class="info"><?php echo timetodate($addtime, 3);?>&nbsp;&nbsp;点击:<?php echo $hits;?></div>
<div class="content"><?php echo $content;?></div>
</div>
<?php } else { ?>
<?php if($lists) { ?>
<ul class="list-txt">
<?php if(is_array($lists)) { foreach($lists as $v) { ?>
<li><span><?php echo $v['date'];?></span><a href="<?php echo $v['linkurl'];?>"><?php echo $v['title'];?></a></li>
<?php } } ?>
</ul>
<?php } else { ?>
<?php include template('empty', 'chip');?>
<?php } ?>
<?php } ?>
<?php if($pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>
<?php include template('footer', $template);?>