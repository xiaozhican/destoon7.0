<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header', $template);?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>" data-direction="reverse"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
<div class="head-bar-right">
<a href="javascript:window.scrollTo(0,0);$('#type-<?php echo $file;?>').slideToggle(300);"><img src="<?php echo DT_MOB;?>static/img/icon-action.png" width="24" height="24"/></a>
</div>
</div>
<div class="head-bar-fix"></div>
</div>
<div id="type-<?php echo $file;?>" style="display:none;">
<div class="list-set">
<ul>
<li><div><a href="<?php echo userurl($username, 'file=credit', $domain);?>">公司档案</a></div></li>
<li><div><a href="<?php echo userurl($username, 'file=contact', $domain);?>">联系方式</a></div></li>
<?php if(is_array($TYPE)) { foreach($TYPE as $k => $v) { ?>
<li<?php if($itemid==$v['itemid']) { ?> style="background:#F6F6F6;"<?php } ?>
><div><a href="<?php echo $v['linkurl'];?>"><?php echo $v['alt'];?></a></div></li>
<?php } } ?>
</ul>
</div>
<div class="blank-35 bd-b"></div>
</div>
<?php if($itemid) { ?>
<div class="main">
<?php if($content) { ?><div class="content"><?php echo $content;?></div><?php } ?>
</div>
<?php } else { ?>
<div class="main">
<div class="content">
<?php if($video) { ?>
<center><?php echo video5_player($video, 280, 210);?></center>
<?php } ?>
<?php if($thumb) { ?><center><img src="<?php echo $thumb;?>"/></center><?php } ?>
<?php echo $content;?>
</div>
</div>
<?php $itemid = $userid;?>
<?php include template('comment', 'chip');?>
<?php } ?>
<?php include template('footer', $template);?>