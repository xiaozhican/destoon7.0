<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header', $template);?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
</div>
<div class="head-bar-fix"></div>
</div>
<?php if($itemid) { ?>
<div class="main">
<div class="title"><strong><?php echo $title;?></strong></div>
<div class="info"><?php echo $editdate;?><?php if($MOD['hits']) { ?>&nbsp;&nbsp;浏览:<span id="hits"><?php echo $hits;?></span><?php } ?>
</div>
<?php if($pass) { ?>
<div class="album" style="margin:0;">
<div id="album" style="margin:0;"><img src="<?php echo $P['big'];?>" id="photo_<?php echo $itemid;?>" style="width:100%;" onclick="photo_next(<?php echo $itemid;?>)"/></div><div style="line-height:30px;font-size:18px;"><span id="photo_page_<?php echo $itemid;?>"><?php echo $page;?></span> / <?php echo $items;?></div>
</div>
<div style="display:none;">
<?php if(is_array($T)) { foreach($T as $i => $p) { ?>
<div id="image_<?php echo $itemid;?>_<?php echo $i;?>"><?php echo $p['big'];?></div>
<div id="intro_<?php echo $itemid;?>_<?php echo $i;?>"><?php echo $p['introduce'];?></div>
<?php } } ?>
<div id="cur_<?php echo $itemid;?>"><?php echo $page;?></div>
<div id="max_<?php echo $itemid;?>"><?php echo $items;?></div>
</div>
<script type="text/javascript" src="<?php echo DT_MOB;?>static/js/photo.js"></script>
<script type="text/javascript">
$(function(){
$('#photo_<?php echo $itemid;?>').on('swipeleft',function(){
photo_next(<?php echo $itemid;?>);
});
$('#photo_<?php echo $itemid;?>').on('swiperight',function(){
photo_prev(<?php echo $itemid;?>);
});
});
</script>
<div id="photo_intro" class="content"><?php echo $P['introduce'];?></div>
<?php if($content) { ?><div class="content"><?php echo $content;?></div><?php } ?>
<?php } else { ?>
<div class="content">
此<?php echo $MOD['name'];?>需要验证，<?php if($open == 2) { ?>请输入访问密码<?php } else if($open == 1) { ?>请回答<?php echo $question;?><?php } ?>
<form id="photo-form">
<input type="hidden" name="itemid" value="<?php echo $itemid;?>"/>
<input type="hidden" name="action" value="verify"/>
<div style="border:#D8D8D8 1px solid;margin:10px 0;padding:2px 6px;border-radius:4px;"><input type="text" name="key" id="key" style="width:100%;height:28px;line-height:28px;border:none;font-size:14px;padding:0;" onblur="window.scrollTo(0,0);"/></div>
<div class="btn-green" onclick="Dphoto();">立即验证</div>
</form>
</div>
<script type="text/javascript">
function Dphoto() {
if($('#key').val().length < 1) {
Dtoast('请填写验证信息');
return false;
}
$.post('<?php echo $MOD['mobile'];?>show.php', $('#photo-form').serialize(), function(data) {
if(data == 'ok') {
Dtoast('验证成功');
setTimeout(function() {
window.location.reload();
}, 1000);
} else {
Dtoast('验证失败，请重试');
}
});
}
</script>
<?php } ?>
<?php if($MOD['fee_award']) { ?>
<div class="award"><a href="<?php echo $MODULE['2']['mobile'];?>award.php?mid=<?php echo $moduleid;?>&itemid=<?php echo $itemid;?>" rel="external"><div>打赏</div></a></div>
<?php } ?>
</div>
<?php } else { ?>
<?php if($lists) { ?>
<?php if(is_array($lists)) { foreach($lists as $v) { ?>
<div class="list-img">
<a href="<?php echo $v['linkurl'];?>"><img src="<?php if($v['thumb']) { ?><?php echo $v['thumb'];?><?php } else { ?><?php echo DT_MOB;?>static/img/80x60.png<?php } ?>
" width="80" height="60" alt="" onerror="this.src='<?php echo DT_MOB;?>static/img/80x60.png';"/></a>
<ul>
<li><a href="<?php echo $v['linkurl'];?>"><strong><?php echo $v['title'];?></strong></a></li>
<li><em class="f_r"><?php echo timetodate($v['addtime'], 5);?></em><span><?php echo $v['items'];?> 张图片</span></li>
</ul>
</div>
<?php } } ?>
<?php } else { ?>
<?php include template('empty', 'chip');?>
<?php } ?>
<?php if($pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>
<?php } ?>
<?php include template('footer', $template);?>