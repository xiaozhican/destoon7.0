# DESTOON V7.0 R201800509 https://www.destoon.com
# 2019-01-23 10:14:24
# --------------------------------------------------------



DROP TABLE IF EXISTS `destoon_validate`;
CREATE TABLE `destoon_validate` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(30) NOT NULL DEFAULT '',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `thumb1` varchar(255) NOT NULL DEFAULT '',
  `thumb2` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM AUTO_INCREMENT=212 DEFAULT CHARSET=utf8 COMMENT='资料认证';

INSERT INTO `destoon_validate` VALUES('1','重庆市赛普塑料制品有限公司','company','https://www.savt.cn/file/upload/201807/03/10280368414.jpg','https://www.savt.cn/file/upload/201807/03/10150975414.jpg','https://www.savt.cn/file/upload/201807/03/10151858414.jpg','yangyuwen','1530584925','admin','1534978025','222.179.155.92','3');
INSERT INTO `destoon_validate` VALUES('2','山东中扬信息技术有限公司','company','https://www.savt.cn/file/upload/201807/03/11580245419.jpg','','','zhongyangapp','1530590301','admin','1534978025','113.122.238.142','3');
INSERT INTO `destoon_validate` VALUES('3','梁女士','truename','https://www.savt.cn/file/upload/201807/03/13553064421.jpg','','','fhy168','1530597339','admin','1534978034','101.72.37.138','3');
INSERT INTO `destoon_validate` VALUES('4','天津市江瑞阀门贸易有限公司','company','https://www.savt.cn/file/upload/201807/03/16151633424.jpg','','','jrfmxiaoqiao1','1530605723','admin','1534978025','125.37.71.171','3');
INSERT INTO `destoon_validate` VALUES('5','深圳德平电子有限公司','company','https://www.savt.cn/file/upload/201807/03/16233121425.jpg','','','depingdianzi','1530606234','admin','1534978025','113.87.55.69','3');
INSERT INTO `destoon_validate` VALUES('6','张妍妍','truename','https://www.savt.cn/file/upload/201807/03/16243241425.jpg','https://www.savt.cn/file/upload/201807/03/16243978425.jpg','','depingdianzi','1530606289','admin','1534978034','113.87.55.69','3');
INSERT INTO `destoon_validate` VALUES('7','山东天朗环保科技有限公司','company','https://www.savt.cn/file/upload/201807/04/09031916428.jpg','','','xuxiaojie666','1530666207','admin','1534978025','120.192.185.114','3');
INSERT INTO `destoon_validate` VALUES('8','深圳市铂胜光电有限公司','company','https://www.savt.cn/file/upload/201807/04/15263557435.jpg','','','bersn2017','1530689202','admin','1534978025','119.123.2.89','3');
INSERT INTO `destoon_validate` VALUES('9','山东鲁之家机械设备有限公司','company','https://www.savt.cn/file/upload/201807/05/15082812448.jpg','','','luhouse','1530774517','admin','1534978025','112.36.49.62','3');
INSERT INTO `destoon_validate` VALUES('10','福建兴翼机械有限公司','company','https://www.savt.cn/file/upload/201807/07/09341791460.jpg','','','xingyijixie','1530927267','admin','1534978025','202.101.147.222','3');
INSERT INTO `destoon_validate` VALUES('11','杭州佳洁机电设备有限公司','company','https://www.savt.cn/file/upload/201807/08/08584315467.jpg','','','hz8845','1531011529','admin','1534978025','125.121.153.96','3');
INSERT INTO `destoon_validate` VALUES('12','朱关胜','truename','https://www.savt.cn/file/upload/201807/08/08593731467.jpg','','','hz8845','1531011583','admin','1534978034','125.121.153.96','3');
INSERT INTO `destoon_validate` VALUES('13','王滨','truename','https://www.savt.cn/file/upload/201807/08/15240463470.png','','','b520j1985','1531034649','admin','1534978034','218.89.43.0','3');
INSERT INTO `destoon_validate` VALUES('14','北京龙翔旭辉科技有限公司','company','https://www.savt.cn/file/upload/201807/08/15241783470.jpg','','','b520j1985','1531034662','admin','1534978025','218.89.43.0','3');
INSERT INTO `destoon_validate` VALUES('15','深圳市华莱生物科技有限公司','company','https://www.savt.cn/file/upload/201807/09/16235639481.jpg','','','szhl1802','1531124646','admin','1534978008','119.98.83.169','3');
INSERT INTO `destoon_validate` VALUES('16','洛阳国润新材料科技股份有限公司','company','https://www.savt.cn/file/upload/201807/10/11093753486.jpg','','','gr122','1531192185','admin','1534978008','123.163.22.20','3');
INSERT INTO `destoon_validate` VALUES('17','杭州众邦净化设备有限公司','company','https://www.savt.cn/file/upload/201807/12/11123169518.jpg','','','zb13771570032','1531365158','admin','1534978008','115.204.42.184','3');
INSERT INTO `destoon_validate` VALUES('18','上海惠茗餐饮企业管理有限公司','company','https://www.savt.cn/file/upload/201807/12/15035370506.jpg','','','zzz777','1531379046','admin','1534978008','114.95.241.166','3');
INSERT INTO `destoon_validate` VALUES('19','景德镇市安轩陶瓷有限公司','company','https://www.savt.cn/file/upload/201807/12/15443835523.jpg','','','axtc01','1531381485','admin','1534978008','39.167.116.47','3');
INSERT INTO `destoon_validate` VALUES('20','霸州市霸州镇万齐电力器材厂','company','https://www.savt.cn/file/upload/201807/12/17283917525.jpg','','','zhijin333','1531387738','admin','1534978008','124.238.176.116','3');
INSERT INTO `destoon_validate` VALUES('21','郑克超','truename','https://www.savt.cn/file/upload/201807/14/10305340546.jpg','https://www.savt.cn/file/upload/201807/14/10310131546.jpg','','jiuxing17688','1531535468','admin','1534978034','113.70.239.134','3');
INSERT INTO `destoon_validate` VALUES('22','北京天兴陶瓷复合材料有限公司','company','https://www.savt.cn/file/upload/201807/18/15200790582.jpg','','','tianxingtc','1531898421','admin','1534978008','221.219.180.216','3');
INSERT INTO `destoon_validate` VALUES('23','深圳市雷乐联盈科技有限公司','company','https://www.savt.cn/file/upload/201807/19/08512287561.png','','','leilawin139','1531961486','admin','1534978008','120.86.58.24','3');
INSERT INTO `destoon_validate` VALUES('24','王峰','truename','https://www.savt.cn/file/upload/201807/20/17421633612.jpg','','','jb1688661','1532079746','admin','1534978034','119.185.42.113','3');
INSERT INTO `destoon_validate` VALUES('25','山东京邦电器有限公司','company','https://www.savt.cn/file/upload/201807/20/17423520612.jpg','','','jb1688661','1532079763','admin','1534978008','119.185.42.113','3');
INSERT INTO `destoon_validate` VALUES('26','郑州市清远教育咨询有限公司','company','https://www.savt.cn/file/upload/201807/22/10093159625.jpg','','','zhuohuajiaoyu','1532225382','admin','1534978008','1.194.22.10','3');
INSERT INTO `destoon_validate` VALUES('27','黑龙江磐云建筑材料有限公司','company','https://www.savt.cn/file/upload/201807/22/16313992630.jpg','','','huaqianjinglang','1532248308','admin','1534978008','113.4.6.225','3');
INSERT INTO `destoon_validate` VALUES('28','济宁恒康生物医药有限公司','company','https://www.savt.cn/file/upload/201807/24/19321167532.jpg','','','sdhksw','1532431959','admin','1534978008','182.42.197.28','3');
INSERT INTO `destoon_validate` VALUES('29','3261833368@qq.com','email','','','','sdhksw','1532432076','system','1532432076','182.42.197.28','3');
INSERT INTO `destoon_validate` VALUES('30','黄会','truename','https://www.savt.cn/file/upload/201807/24/19351055532.jpg','https://www.savt.cn/file/upload/201807/24/19351841532.jpg','','sdhksw','1532432125','admin','1534978034','182.42.197.28','3');
INSERT INTO `destoon_validate` VALUES('31','东莞市富朗特绝缘材料有限公司','company','https://www.savt.cn/file/upload/201807/27/17045572682.png','','','front100','1532682303','admin','1534978008','14.220.237.194','3');
INSERT INTO `destoon_validate` VALUES('32','东莞市帝邦塑料有限公司','company','https://www.savt.cn/file/upload/201807/27/21175172683.jpg','','','liuliangmei1688','1532697484','admin','1534978008','27.44.238.178','3');
INSERT INTO `destoon_validate` VALUES('33','liuliangmei1688@163.com','email','','','','liuliangmei1688','1532697703','system','1532697703','27.44.238.178','3');
INSERT INTO `destoon_validate` VALUES('34','中山联久生物科技有限公司','company','https://www.savt.cn/file/upload/201807/28/10591490687.jpg','','','wangzhaowei1477','1532746761','admin','1534978008','14.211.3.67','3');
INSERT INTO `destoon_validate` VALUES('35','枣强县燕航复合材料厂','company','https://www.savt.cn/file/upload/201808/01/09040664699.jpg','','','sf15713186169','1533085456','admin','1534978008','120.15.246.186','3');
INSERT INTO `destoon_validate` VALUES('36','刘忠燕','truename','https://www.savt.cn/file/upload/201808/01/09044183699.jpg','','','sf15713186169','1533085489','admin','1534978034','120.15.246.186','3');
INSERT INTO `destoon_validate` VALUES('37','深圳市鸿栢科技实业有限公司','company','https://www.savt.cn/file/upload/201808/01/13572660705.jpg','','','sawchina','1533103064','admin','1534978008','113.89.122.171','3');
INSERT INTO `destoon_validate` VALUES('38','孙辉','truename','https://www.savt.cn/file/upload/201808/01/13582571705.jpg','https://www.savt.cn/file/upload/201808/01/13583276705.jpg','','sawchina','1533103131','admin','1534978034','113.89.122.171','3');
INSERT INTO `destoon_validate` VALUES('39','刘亮','truename','https://www.savt.cn/file/upload/201808/02/15433318433.jpg','','','shierbanna','1533195818','admin','1534978034','119.62.209.191','3');
INSERT INTO `destoon_validate` VALUES('40','西双版纳景洪市十二风情电子商务有限责任公司','company','https://www.savt.cn/file/upload/201808/02/15434862433.jpg','','','shierbanna','1533195837','admin','1534978008','119.62.209.191','3');
INSERT INTO `destoon_validate` VALUES('41','378709641@qq.com','email','','','','shierbanna','1533195900','system','1533195900','119.62.209.191','3');
INSERT INTO `destoon_validate` VALUES('42','博亿成科技服务（武汉）有限公司','company','https://www.savt.cn/file/upload/201808/03/17565153728.jpg','','','boyicheng03','1533290219','admin','1534978008','219.139.214.216','3');
INSERT INTO `destoon_validate` VALUES('43','381133141@qq.com','email','','','','tyfhtjx','1533367046','system','1533367046','113.109.197.220','3');
INSERT INTO `destoon_validate` VALUES('44','灵璧县四合包装材料经营部','company','https://www.savt.cn/file/upload/201808/05/15534352737.jpg','','','shbz2018','1533455630','admin','1534978008','223.214.136.97','3');
INSERT INTO `destoon_validate` VALUES('45','朱成法','truename','https://www.savt.cn/file/upload/201808/05/15535954737.jpg','https://www.savt.cn/file/upload/201808/05/15540366737.jpg','','shbz2018','1533455650','admin','1534978034','223.214.136.97','3');
INSERT INTO `destoon_validate` VALUES('46','泰州市锋发动力设备有限公司','company','https://www.savt.cn/file/upload/201808/06/16051329752.jpg','','','kffdjz','1533542722','admin','1534977990','221.230.165.97','3');
INSERT INTO `destoon_validate` VALUES('47','汪凌宇','truename','https://www.savt.cn/file/upload/201808/08/22474132783.jpg','','','wly909754','1533739667','admin','1534978034','14.211.1.140','3');
INSERT INTO `destoon_validate` VALUES('48','中山联久生物科技有限公司业务部','company','https://www.savt.cn/file/upload/201808/08/22475944783.jpg','','','wly909754','1533739683','admin','1534977990','14.211.1.140','3');
INSERT INTO `destoon_validate` VALUES('49','江苏英特派奇包装制品有限公司','company','https://www.savt.cn/file/upload/201808/09/07341472781.jpg','','','daiwenyu8866','1533771264','admin','1534977990','117.85.93.139','3');
INSERT INTO `destoon_validate` VALUES('50','玖德隆机械（昆山）有限公司','company','https://www.savt.cn/file/upload/201808/09/10264444742.jpg','','','jdl1217','1533781616','admin','1534977990','112.3.220.236','3');
INSERT INTO `destoon_validate` VALUES('51','广东源磊粉体有限公司','company','https://www.savt.cn/file/upload/201808/13/14470681546.jpg','https://www.savt.cn/file/upload/201808/13/14471334546.jpg','https://www.savt.cn/file/upload/201808/13/14472228546.jpg','jiuxing17688','1534142849','admin','1534977990','125.95.197.95','3');
INSERT INTO `destoon_validate` VALUES('52','1218229767@qq.com','email','','','','jiuxing17688','1534143080','system','1534143080','125.95.197.95','3');
INSERT INTO `destoon_validate` VALUES('53','四川区间卫家信息科技有限公司','company','https://www.savt.cn/file/upload/201808/15/09201275790.jpg','','','qujiankeji','1534296033','admin','1534977990','117.176.235.127','3');
INSERT INTO `destoon_validate` VALUES('54','上海禹轩泵阀有限公司','company','https://www.savt.cn/file/upload/201808/15/12561629835.jpg','','','yxvalve','1534308992','admin','1534977990','45.115.219.82','3');
INSERT INTO `destoon_validate` VALUES('55','919991329@qq.com','email','','','','waz888','1534313274','system','1534313274','223.155.85.174','3');
INSERT INTO `destoon_validate` VALUES('56','英德市伯克力涂料有限公司','company','https://www.savt.cn/file/upload/201808/15/15322297839.jpg','','','fl333','1534318354','admin','1534977990','120.239.192.60','3');
INSERT INTO `destoon_validate` VALUES('57','周凤莲','truename','https://www.savt.cn/file/upload/201808/15/15324931839.jpg','','','fl333','1534318395','admin','1534978034','120.239.192.60','3');
INSERT INTO `destoon_validate` VALUES('58','13750132112@163.com','email','','','','fl333','1534318480','system','1534318480','120.239.192.60','3');
INSERT INTO `destoon_validate` VALUES('59','上海沃驰实业有限公司','company','https://www.savt.cn/file/upload/201808/16/16181397496.png','','','vohcl123','1534407502','admin','1534977990','61.165.191.120','3');
INSERT INTO `destoon_validate` VALUES('60','山东正通土工材料有限公司','company','https://www.savt.cn/file/upload/201808/16/17025496854.jpg','','','sdzttmcl01','1534410179','admin','1534977990','113.88.47.207','3');
INSERT INTO `destoon_validate` VALUES('61','中山市铭德检测技术服务有限公司','company','https://www.savt.cn/file/upload/201808/17/10063365856.jpg','','','mingde18','1534471600','admin','1534977990','120.239.180.5','3');
INSERT INTO `destoon_validate` VALUES('62','东莞市畅鼎香茶叶有限公司','company','https://www.savt.cn/file/upload/201808/17/14173321862.jpg','','','cdx17180387879','1534486675','admin','1534977990','27.44.35.136','3');
INSERT INTO `destoon_validate` VALUES('63','3003861508@qq.com','email','','','','wangzhaowei1477','1534571778','system','1534571778','116.28.51.19','3');
INSERT INTO `destoon_validate` VALUES('64','深圳市鑫永豪科技有限公司','company','https://www.savt.cn/file/upload/201808/18/14535767873.jpg','','','szyh007','1534575243','admin','1534977990','113.88.108.56','3');
INSERT INTO `destoon_validate` VALUES('65','江西华罡网络营销策划有限公司','company','https://www.savt.cn/file/upload/201808/18/15472652874.jpg','','','myu008','1534578454','admin','1534977990','59.53.31.151','3');
INSERT INTO `destoon_validate` VALUES('66','酷肯空气净化技术（上海）有限公司','company','https://www.savt.cn/file/upload/201808/18/15595772875.jpg','','','skflife','1534579217','admin','1534977990','183.128.150.31','3');
INSERT INTO `destoon_validate` VALUES('67','海牙认证服务（广州）有限公司','company','https://www.savt.cn/file/upload/201808/20/14023982882.jpg','','','gdapostille','1534744964','admin','1534977990','120.239.180.78','3');
INSERT INTO `destoon_validate` VALUES('68','广州市碧峰环境科技有限公司','company','https://www.savt.cn/file/upload/201808/21/14514041895.jpg','','','as2018003','1534834329','admin','1534977990','183.6.159.35','3');
INSERT INTO `destoon_validate` VALUES('69','淄博恒鼎风机有限公司','company','https://www.savt.cn/file/upload/201808/22/10321537899.jpg','','','zibofj','1534905142','admin','1534977990','119.181.232.145','3');
INSERT INTO `destoon_validate` VALUES('70','475662603@qq.com','email','','','','zibofj','1534905266','system','1534905266','119.181.232.145','3');
INSERT INTO `destoon_validate` VALUES('71','天津市建明液压机械有限公司','company','https://www.savt.cn/file/upload/201808/22/17125692903.jpg','','','toushuizhuanji','1534929184','admin','1534977990','60.27.188.157','3');
INSERT INTO `destoon_validate` VALUES('72','济宁市安源机械设备有限公司','company','https://www.savt.cn/file/upload/201808/22/17201392904.jpg','','','jnanyuan03','1534929618','admin','1534977990','123.132.79.80','3');
INSERT INTO `destoon_validate` VALUES('73','杭州金键智能科技有点公司','company','https://www.savt.cn/file/upload/201808/23/09540729522.jpg','','','hzjjzn','1534989254','admin','1536201038','122.225.218.82','3');
INSERT INTO `destoon_validate` VALUES('74','宁津县顺展网链厂','company','https://www.savt.cn/file/upload/201808/23/11245445907.jpg','https://www.savt.cn/file/upload/201808/23/11250678907.jpg','https://www.savt.cn/file/upload/201808/23/11251345907.jpg','shunzhan2018','1534994721','admin','1536201038','39.74.25.66','3');
INSERT INTO `destoon_validate` VALUES('75','3177781423@qq.com','email','','','','shunzhan2018','1534995124','system','1534995124','39.74.25.66','3');
INSERT INTO `destoon_validate` VALUES('76','赛珂（厦门）建材有限公司','company','https://www.savt.cn/file/upload/201808/23/15503835865.jpg','','','sinodec','1535010645','admin','1536201038','120.37.181.99','3');
INSERT INTO `destoon_validate` VALUES('77','深圳雷乐联盈科技有限公司','company','https://www.savt.cn/file/upload/201808/24/15011868564.png','','','leilawin2820','1535094089','admin','1536201038','120.86.58.78','3');
INSERT INTO `destoon_validate` VALUES('78','广州品漉高尔夫用品有限公司','company','https://www.savt.cn/file/upload/201808/24/15315776818.jpg','','','brandoogolf','1535095956','admin','1536201038','113.109.207.56','3');
INSERT INTO `destoon_validate` VALUES('79','巩义市亨运机械设备贸易有限公司','company','https://www.savt.cn/file/upload/201808/27/21595725936.jpg','','','hengyunjixie0619','1535378406','admin','1536201038','1.193.119.238','3');
INSERT INTO `destoon_validate` VALUES('80','浙江尚为照明有限公司','company','https://www.savt.cn/file/upload/201808/28/09500558594.jpg','','','sw8160','1535421027','admin','1536201038','60.180.124.132','3');
INSERT INTO `destoon_validate` VALUES('81','13585908956@qq.com','email','','','','vohcl123','1535512513','system','1535512513','61.165.191.120','3');
INSERT INTO `destoon_validate` VALUES('82','罗凤娣','truename','https://www.savt.cn/file/upload/201808/29/16582226954.jpg','','','hangc2018','1535533110','admin','1536201054','14.120.42.159','3');
INSERT INTO `destoon_validate` VALUES('83','深圳市航彩化工有限公司','company','https://www.savt.cn/file/upload/201808/29/16584362954.jpg','','','hangc2018','1535533140','admin','1536201038','14.120.42.159','3');
INSERT INTO `destoon_validate` VALUES('84','m13538313237@163.com','email','','','','hangc2018','1535533174','system','1535533174','14.120.42.159','3');
INSERT INTO `destoon_validate` VALUES('85','2792296018@qq.com','email','','','','dn2792296018','1535606864','system','1535606864','115.235.177.199','3');
INSERT INTO `destoon_validate` VALUES('86','浙江迪能电气科技有限公司','company','https://www.savt.cn/file/upload/201808/30/13282948961.jpg','https://www.savt.cn/file/upload/201808/30/13283418961.jpg','https://www.savt.cn/file/upload/201808/30/13284156961.jpg','dn2792296018','1535606931','admin','1536201038','115.235.177.199','3');
INSERT INTO `destoon_validate` VALUES('87','济宁东达机电有限责任公司','company','https://www.savt.cn/file/upload/201808/31/08272922942.jpg','','','dongda99','1535675260','admin','1536201038','112.236.35.69','3');
INSERT INTO `destoon_validate` VALUES('88','广州市优耐检测技术有限公司','company','https://www.savt.cn/file/upload/201808/31/09565868969.png','','','unijenny2018-tam','1535680625','admin','1536201038','61.144.175.117','3');
INSERT INTO `destoon_validate` VALUES('89','青岛亿生泰环保能源科技有限公司','company','https://www.savt.cn/file/upload/201809/03/14315696987.jpg','','','yishengtai1688','1535956328','admin','1536201038','27.210.190.203','3');
INSERT INTO `destoon_validate` VALUES('90','徐志艮','truename','https://www.savt.cn/file/upload/201809/03/14324148987.jpg','https://www.savt.cn/file/upload/201809/03/14324537987.jpg','','yishengtai1688','1535956371','admin','1536201054','27.210.190.203','3');
INSERT INTO `destoon_validate` VALUES('91','华士光照明湖北有限公司','company','https://www.savt.cn/file/upload/201809/03/15045499802.jpg','','','yhsgzm','1535958304','admin','1536201038','111.47.190.109','3');
INSERT INTO `destoon_validate` VALUES('92','黄体君','truename','https://www.savt.cn/file/upload/201809/05/204813461008.jpg','','','oubao7040gg','1536151707','admin','1536201054','27.220.240.53','3');
INSERT INTO `destoon_validate` VALUES('93','上海仡维实业有限公司研发部','company','https://www.savt.cn/file/upload/201809/05/204850871008.jpg','','','oubao7040gg','1536151910','admin','1536201038','27.220.240.53','3');
INSERT INTO `destoon_validate` VALUES('94','江阴市德仁毛纺织科技有限公司','company','https://www.savt.cn/file/upload/201809/07/093040501015.jpg','','','jiangyinderen','1536283854','admin','1537146037','114.223.203.222','3');
INSERT INTO `destoon_validate` VALUES('95','薛广哲','truename','https://www.savt.cn/file/upload/201809/07/101430391016.jpg','','','yw2018','1536286489','admin','1537146021','27.220.240.53','3');
INSERT INTO `destoon_validate` VALUES('96','程清丽','truename','https://www.savt.cn/file/upload/201809/09/155555151030.jpg','https://www.savt.cn/file/upload/201809/09/155601301030.jpg','https://www.savt.cn/file/upload/201809/09/155612871030.jpg','ymjc88','1536479789','admin','1537146021','180.118.16.172','3');
INSERT INTO `destoon_validate` VALUES('97','丹阳市优美建材有限公司','company','https://www.savt.cn/file/upload/201809/09/155704351030.jpg','https://www.savt.cn/file/upload/201809/09/155708771030.jpg','https://www.savt.cn/file/upload/201809/09/155713931030.jpg','ymjc88','1536479844','admin','1537146037','180.118.16.172','3');
INSERT INTO `destoon_validate` VALUES('98','潍坊帕尔曼粉体设备有限公司','company','https://www.savt.cn/file/upload/201809/10/165544941043.jpg','','','pem2018','1536569755','admin','1537146037','119.183.194.77','3');
INSERT INTO `destoon_validate` VALUES('99','邓州市汇邦生物科技有限公司','company','https://www.savt.cn/file/upload/201809/11/144101941055.jpg','','','huibang1123','1536648069','admin','1537146037','219.155.183.131','3');
INSERT INTO `destoon_validate` VALUES('100','上海缘江化工有限公司','company','https://www.savt.cn/file/upload/201809/11/154206871056.jpg','','','ymzh01','1536651734','admin','1537146037','101.86.131.33','3');
INSERT INTO `destoon_validate` VALUES('101','13601840452@163.com','email','','','','ymzh01','1536651780','system','1536651780','101.86.131.33','3');
INSERT INTO `destoon_validate` VALUES('102','成都旺高科技有限公司','company','https://www.savt.cn/file/upload/201809/11/170441881059.jpg','','','z1992','1536656697','admin','1537146037','110.184.60.152','3');
INSERT INTO `destoon_validate` VALUES('103','山东凯发钢铁有限公司','company','https://www.savt.cn/file/upload/201809/11/211129601062.jpg','','','hyxygg','1536671501','admin','1537146037','112.37.129.223','3');
INSERT INTO `destoon_validate` VALUES('104','山东方格医疗器械有限公司','company','https://www.savt.cn/file/upload/201809/13/150013211077.jpg','','','sdfangge','1536822022','admin','1537146037','112.239.232.200','3');
INSERT INTO `destoon_validate` VALUES('105','广东惠华宏业陶瓷科技有限公司','company','https://www.savt.cn/file/upload/201809/13/170155641078.jpg','','','huihua123','1536829963','admin','1537146037','218.20.118.49','3');
INSERT INTO `destoon_validate` VALUES('106','河南耀诺实业有限公司','company','https://www.savt.cn/file/upload/201809/14/140149501052.jpg','','','yaonuoshiye','1536904918','admin','1537146037','123.160.166.141','3');
INSERT INTO `destoon_validate` VALUES('107','中山市盛驰电子科技有限公司','company','https://www.savt.cn/file/upload/201809/14/162705701091.jpg','','','zshengchi','1536913636','admin','1537146037','183.199.163.217','3');
INSERT INTO `destoon_validate` VALUES('108','陈赖','truename','https://www.savt.cn/file/upload/201809/14/162726551091.jpg','','','zshengchi','1536913654','admin','1537146021','183.199.163.217','3');
INSERT INTO `destoon_validate` VALUES('109','谢绍仰','truename','https://www.savt.cn/file/upload/201809/15/162425211104.jpg','https://www.savt.cn/file/upload/201809/15/162528561104.jpg','https://www.savt.cn/file/upload/201809/15/162541821104.jpg','a13630153030','1536999957','admin','1537146021','119.123.203.152','3');
INSERT INTO `destoon_validate` VALUES('110','604175855@qq.com','email','','','','a13630153030','1537000010','system','1537000010','119.123.203.152','3');
INSERT INTO `destoon_validate` VALUES('111','福州腾远生物科技有限公司','company','https://www.savt.cn/file/upload/201809/16/112222271096.jpg','','','lxn2018621','1537068150','admin','1537146037','110.87.165.36','3');
INSERT INTO `destoon_validate` VALUES('112','晋中晋阳湖生物科技有限公司','company','https://www.savt.cn/file/upload/201809/20/095209891102.jpg','https://www.savt.cn/file/upload/201809/20/095215431102.jpg','','jinyanglaker','1537408342','admin','1538183952','60.223.135.220','3');
INSERT INTO `destoon_validate` VALUES('113','常毅','truename','https://www.savt.cn/file/upload/201809/20/095235681102.jpg','https://www.savt.cn/file/upload/201809/20/095239121102.jpg','','jinyanglaker','1537408364','admin','1539310778','60.223.135.220','3');
INSERT INTO `destoon_validate` VALUES('114','霸州市开发区嘉铭滤清器厂','company','https://www.savt.cn/file/upload/201809/20/104005721135.jpg','','','jiaminglvye','1537411212','admin','1538183952','27.189.145.34','3');
INSERT INTO `destoon_validate` VALUES('115','惠州合艺工艺制品有限公司','company','https://www.savt.cn/file/upload/201809/20/143245281140.jpg','','','hyx888','1537425178','admin','1538183952','113.68.41.3','3');
INSERT INTO `destoon_validate` VALUES('116','泗水泓泰包装有限公司','company','https://www.savt.cn/file/upload/201809/20/161844931142.jpg','','','han198211','1537431535','admin','1538183952','112.235.54.29','3');
INSERT INTO `destoon_validate` VALUES('117','上海青沃实业有限公司','company','https://www.savt.cn/file/upload/201809/25/162931951167.jpg','','','shqwsy','1537864178','admin','1538183952','113.123.217.240','3');
INSERT INTO `destoon_validate` VALUES('118','刘玉','truename','https://www.savt.cn/file/upload/201809/25/162948211167.jpg','','','shqwsy','1537864204','admin','1539310778','113.123.217.240','3');
INSERT INTO `destoon_validate` VALUES('119','2457870936@qq.com','email','','','','shqwsy','1537864244','system','1537864244','113.123.217.240','3');
INSERT INTO `destoon_validate` VALUES('120','董祖文','truename','https://www.savt.cn/file/upload/201809/25/181129751168.jpg','https://www.savt.cn/file/upload/201809/25/181136471168.jpg','','gzlxbl','1537870305','admin','1539310778','218.20.7.238','3');
INSERT INTO `destoon_validate` VALUES('121','3140739681@qq.com','email','','','','gzlxbl','1537870461','system','1537870461','218.20.7.238','3');
INSERT INTO `destoon_validate` VALUES('122','广州市乐鑫玻璃制品有限公司','company','https://www.savt.cn/file/upload/201809/25/181445701168.jpg','','','gzlxbl','1537870494','admin','1538183952','218.20.7.238','3');
INSERT INTO `destoon_validate` VALUES('123','陈秀波','truename','https://www.savt.cn/file/upload/201809/28/120143221184.jpg','https://www.savt.cn/file/upload/201809/28/120154831184.jpg','','szvcipack','1538107320','admin','1539310778','183.211.218.148','3');
INSERT INTO `destoon_validate` VALUES('124','苏州气相防锈包装材料有限公司','company','https://www.savt.cn/file/upload/201809/28/120211121184.jpg','','','szvcipack','1538107336','admin','1538183952','183.211.218.148','3');
INSERT INTO `destoon_validate` VALUES('125','18913149994@163.com','email','','','','szvcipack','1538107364','system','1538107364','183.211.218.148','3');
INSERT INTO `destoon_validate` VALUES('126','北京华镁欣业电子设备有限公司','company','https://www.savt.cn/file/upload/201809/28/141912721185.jpg','','','xhhsjs','1538115565','admin','1538183952','119.250.196.203','3');
INSERT INTO `destoon_validate` VALUES('127','西安昌岳生物科技有限公司','company','https://www.savt.cn/file/upload/201809/28/163528651188.jpg','','','xachangyue','1538123737','admin','1538183952','123.139.25.92','3');
INSERT INTO `destoon_validate` VALUES('128','庆云县鹏腾塑料制品有限公司','company','https://www.savt.cn/file/upload/201809/28/231630441190.jpg','','','qyxptsl','1538147801','admin','1538183952','119.183.29.10','3');
INSERT INTO `destoon_validate` VALUES('129','王胜国','truename','https://www.savt.cn/file/upload/201809/28/231652581190.jpg','','','qyxptsl','1538147818','admin','1539310778','119.183.29.10','3');
INSERT INTO `destoon_validate` VALUES('130','1375221239@qq.com','email','','','','xincy02','1538289159','system','1538289159','223.74.101.131','3');
INSERT INTO `destoon_validate` VALUES('131','新长远（深圳）科技有限公司','company','https://www.savt.cn/file/upload/201809/30/143300601199.jpg','','','xincy02','1538289194','admin','1539310800','223.74.101.131','3');
INSERT INTO `destoon_validate` VALUES('132','凌福强','truename','https://www.savt.cn/file/upload/201810/09/133733791226.jpg','','','ruanjian4896','1539063465','admin','1539310778','123.166.129.242','3');
INSERT INTO `destoon_validate` VALUES('133','深圳市小同大美科技有限公司','company','https://www.savt.cn/file/upload/201810/11/162519251104.jpg','https://www.savt.cn/file/upload/201810/11/162526981104.jpg','https://www.savt.cn/file/upload/201810/11/162538831104.jpg','a13630153030','1539246368','admin','1539310800','119.123.202.218','3');
INSERT INTO `destoon_validate` VALUES('134','息冬梅','truename','https://www.savt.cn/file/upload/201810/11/163543401245.jpg','https://www.savt.cn/file/upload/201810/11/163549491245.jpg','','baihegongcheng','1539246956','admin','1539310778','123.180.248.52','3');
INSERT INTO `destoon_validate` VALUES('135','河北百贺工程橡胶有限公司','company','https://www.savt.cn/file/upload/201810/11/163616711245.jpg','','','baihegongcheng','1539246983','admin','1539310800','123.180.248.52','3');
INSERT INTO `destoon_validate` VALUES('136','衡水翔远机电设备有限公司','company','https://www.savt.cn/file/upload/201810/12/111123861248.jpg','','','xiangyuanjn','1539313889','admin','1540256268','183.199.162.1','3');
INSERT INTO `destoon_validate` VALUES('137','芦建杰','truename','https://www.savt.cn/file/upload/201810/12/111143161248.jpg','','','xiangyuanjn','1539313914','admin','1539677970','183.199.162.1','3');
INSERT INTO `destoon_validate` VALUES('138','济宁和丰工程机械有限公司','company','https://www.savt.cn/file/upload/201810/14/084839151256.jpg','','','hefeng158','1539478131','admin','1540256268','182.42.192.55','3');
INSERT INTO `destoon_validate` VALUES('139','佛山市南海区威森机械厂','company','https://www.savt.cn/file/upload/201810/14/110104691257.jpg','https://www.savt.cn/file/upload/201810/14/110112231257.jpg','https://www.savt.cn/file/upload/201810/14/110117261257.jpg','ws17708670852','1539486103','admin','1540256268','113.105.200.87','3');
INSERT INTO `destoon_validate` VALUES('140','陈永肖','truename','https://www.savt.cn/file/upload/201810/14/110907361257.jpg','https://www.savt.cn/file/upload/201810/14/110913231257.jpg','https://www.savt.cn/file/upload/201810/14/110918621257.jpg','ws17708670852','1539486587','admin','1539677970','113.105.200.87','3');
INSERT INTO `destoon_validate` VALUES('141','薄海宽','truename','https://www.savt.cn/file/upload/201810/15/111405271261.jpg','','','w389067224','1539573255','admin','1539677970','115.60.142.240','3');
INSERT INTO `destoon_validate` VALUES('142','雄霸电力有限公司','company','https://www.savt.cn/file/upload/201810/16/135619471272.jpg','','','chxiongbadianli','1539669398','admin','1540256268','60.180.141.199','3');
INSERT INTO `destoon_validate` VALUES('143','971122191@qq.com','email','','','','chxiongbadianli','1539669491','system','1539669491','60.180.141.199','3');
INSERT INTO `destoon_validate` VALUES('144','王博','truename','https://www.savt.cn/file/upload/201810/17/104806721279.jpg','','','mingdingxj','1539744493','admin','1540256236','123.180.248.52','3');
INSERT INTO `destoon_validate` VALUES('145','衡水铭鼎工程橡胶有限公司','company','https://www.savt.cn/file/upload/201810/17/104822431279.jpg','','','mingdingxj','1539744508','admin','1540256268','123.180.248.52','3');
INSERT INTO `destoon_validate` VALUES('146','西安三摩地瑜伽服务有限公司','company','https://www.savt.cn/file/upload/201810/17/153225401280.jpg','','','sanmodi123','1539761585','admin','1540256268','36.40.41.216','3');
INSERT INTO `destoon_validate` VALUES('147','胡朕恺','truename','https://www.savt.cn/file/upload/201810/18/150329951288.jpg','','','yangyang7926','1539846217','admin','1540256236','115.217.210.5','3');
INSERT INTO `destoon_validate` VALUES('148','安平县华莱金属丝网有限公司','company','https://www.savt.cn/file/upload/201810/19/131208131292.jpg','','','hualai166','1539925937','admin','1540256268','121.17.206.210','3');
INSERT INTO `destoon_validate` VALUES('149','深圳市衣叁唯品服饰有限公司','company','https://www.savt.cn/file/upload/201810/22/114631181258.jpg','','','yisanweipin','1540180002','admin','1540256268','182.200.70.61','3');
INSERT INTO `destoon_validate` VALUES('150','济南鑫顺源包装有限公司','company','https://www.savt.cn/file/upload/201810/24/162249591326.jpg','','','shantina123','1540369381','admin','1540801054','120.192.11.215','3');
INSERT INTO `destoon_validate` VALUES('151','石家庄久佳化工有限公司','company','https://www.savt.cn/file/upload/201810/25/074856611322.jpg','','','jiujiahuagong','1540424946','admin','1540801054','120.0.98.137','3');
INSERT INTO `destoon_validate` VALUES('152','创新维深圳电子有限公司','company','https://www.savt.cn/file/upload/201810/26/112334941250.jpg','','','h18926088900','1540524224','admin','1540801054','113.91.141.109','3');
INSERT INTO `destoon_validate` VALUES('153','北京博亚国际展览有限公司','company','https://www.savt.cn/file/upload/201810/29/114237821117.jpg','','','li456789','1540784567','admin','1540801054','182.116.51.188','3');
INSERT INTO `destoon_validate` VALUES('154','济南纯一装饰工程有限公司','company','https://www.savt.cn/file/upload/201810/30/112233631363.jpg','','','chunyizhuangshi','1540869761','admin','1541684423','39.78.125.3','3');
INSERT INTO `destoon_validate` VALUES('155','新乡市德隆化工有限公司','company','https://www.savt.cn/file/upload/201810/30/134704181364.jpg','','','xxdlhg','1540878430','admin','1541684423','223.91.218.135','3');
INSERT INTO `destoon_validate` VALUES('156','无锡垚乾信息咨询有限公司','company','https://www.savt.cn/file/upload/201810/30/190343561370.jpg','','','greezhong','1540897433','admin','1541684423','111.77.229.20','3');
INSERT INTO `destoon_validate` VALUES('157','18250837655@163.com','email','','','','heigu2018','1540974110','system','1540974110','120.41.187.154','3');
INSERT INTO `destoon_validate` VALUES('158','厦门黑谷网络科技有限公司','company','https://www.savt.cn/file/upload/201810/31/162412731375.jpg','','','heigu2018','1540974264','admin','1541684423','120.41.187.154','3');
INSERT INTO `destoon_validate` VALUES('159','深圳市得汇科技有限公司','company','https://www.savt.cn/file/upload/201810/31/163551921376.jpg','','','zmrdhkj','1540974960','admin','1541684423','183.222.190.120','3');
INSERT INTO `destoon_validate` VALUES('160','1556907642@qq.com','email','','','','juyongwei','1541143417','system','1541143417','27.198.153.240','3');
INSERT INTO `destoon_validate` VALUES('161','西安千度网络科技有限公司','company','https://www.savt.cn/file/upload/201811/02/153250441387.png','https://www.savt.cn/file/upload/201811/02/153257481387.png','https://www.savt.cn/file/upload/201811/02/153304401387.png','qianduwl123','1541143990','admin','1541684423','36.40.41.99','3');
INSERT INTO `destoon_validate` VALUES('162','深圳市恒驰机电设备有限公司','company','https://www.savt.cn/file/upload/201811/02/223431431393.jpg','','','szhengchi','1541169277','admin','1541684423','120.229.65.218','3');
INSERT INTO `destoon_validate` VALUES('163','蔡爱如','truename','https://www.savt.cn/file/upload/201811/04/101401641399.jpg','https://www.savt.cn/file/upload/201811/04/101407351399.jpg','','liu15630617588','1541297656','admin','1541684369','101.30.28.140','3');
INSERT INTO `destoon_validate` VALUES('164','河北沃步保温材料有限公司','company','https://www.savt.cn/file/upload/201811/04/101757801399.jpg','','','liu15630617588','1541297898','admin','1541684423','101.30.28.140','3');
INSERT INTO `destoon_validate` VALUES('165','775018098@qq.com','email','','','','liu15630617588','1541298013','system','1541298013','101.30.28.140','3');
INSERT INTO `destoon_validate` VALUES('166','3336692818@qq.com','email','','','','gysh','1541342362','system','1541342362','113.116.112.159','3');
INSERT INTO `destoon_validate` VALUES('167','商成飞','truename','https://www.savt.cn/file/upload/201811/04/223956491403.jpg','','','gysh','1541342406','admin','1541684369','113.116.112.159','3');
INSERT INTO `destoon_validate` VALUES('168','隋秀明','truename','https://www.savt.cn/file/upload/201811/05/143233711370.jpg','','','greezhong','1541399559','admin','1541684369','106.6.79.235','3');
INSERT INTO `destoon_validate` VALUES('169','郑州大智若云网络科技有限公司','company','https://www.savt.cn/file/upload/201811/05/144718111407.jpg','','','ruoyunkeji','1541400463','admin','1541684423','115.57.134.220','3');
INSERT INTO `destoon_validate` VALUES('170','中山市艾航机械设备有限公司','company','https://www.savt.cn/file/upload/201811/06/164841531412.jpg','','','ah1111','1541494130','admin','1541684423','163.179.52.236','3');
INSERT INTO `destoon_validate` VALUES('171','泸州长江慢性病专科医院','company','https://www.savt.cn/file/upload/201811/06/175129471400.jpg','','','cjyy141','1541497895','admin','1541684423','182.129.165.148','3');
INSERT INTO `destoon_validate` VALUES('172','江西省思彤科技发展有限公司','company','https://www.savt.cn/file/upload/201811/07/151553141377.jpg','','','sitong8','1541574967','admin','1541684423','182.85.219.101','3');
INSERT INTO `destoon_validate` VALUES('173','3460574853@qq.com','email','','','','dongling2008','1542000701','system','1542000701','112.95.22.29','3');
INSERT INTO `destoon_validate` VALUES('174','深圳市东凌智能科技有限公司','company','https://www.savt.cn/file/upload/201811/12/133155701446.jpg','','','dongling2008','1542000724','admin','1542266553','112.95.22.29','3');
INSERT INTO `destoon_validate` VALUES('175','山东聚隆矿山设备制造有限公司','company','https://www.savt.cn/file/upload/201811/12/134452511448.jpg','','','sdft023','1542001500','admin','1542266553','119.183.178.198','3');
INSERT INTO `destoon_validate` VALUES('176','924803876@qq.com','email','','','','jhpszdq','1542078807','system','1542078807','42.226.255.157','3');
INSERT INTO `destoon_validate` VALUES('177','韩金举','truename','https://www.savt.cn/file/upload/201811/13/111357491451.jpg','','','jhpszdq','1542078852','admin','1542266548','42.226.255.157','3');
INSERT INTO `destoon_validate` VALUES('178','焦作市晶虹盘式制动器有限公司','company','https://www.savt.cn/file/upload/201811/13/111705941451.jpg','','','jhpszdq','1542079033','admin','1542266553','42.226.255.157','3');
INSERT INTO `destoon_validate` VALUES('179','广州市法曲化妆品有限公司','company','https://www.savt.cn/file/upload/201811/14/153609741460.jpg','','','gzfq0609','1542181003','admin','1542266553','223.198.184.22','3');
INSERT INTO `destoon_validate` VALUES('180','济南新麦烤箱有限公司','company','https://www.savt.cn/file/upload/201811/15/142523371466.jpg','','','njhb1007','1542263129','admin','1542266553','114.222.121.82','3');
INSERT INTO `destoon_validate` VALUES('181','深圳市议题传播有限公司','company','https://www.savt.cn/file/upload/201811/16/102504271469.jpg','','','sjw135','1542335126','admin','1543049045','113.87.183.56','3');
INSERT INTO `destoon_validate` VALUES('182','郑志刚','truename','https://www.savt.cn/file/upload/201811/18/143252431476.jpg','','','q18858975833','1542522779','admin','1543049034','39.186.188.80','3');
INSERT INTO `destoon_validate` VALUES('183','义乌市奇缔丝家纺有限责任公司','company','https://www.savt.cn/file/upload/201811/18/143307881476.jpg','','','q18858975833','1542522792','admin','1543049045','39.186.188.80','3');
INSERT INTO `destoon_validate` VALUES('184','东莞市永绿环保工程有限公司','company','https://www.savt.cn/file/upload/201811/21/134646811492.jpg','','','yongluhuanbao','1542779215','admin','1543049045','58.252.63.146','3');
INSERT INTO `destoon_validate` VALUES('185','石家庄万惠企业管理咨询有限公司','company','https://www.savt.cn/file/upload/201811/22/135016991499.jpg','','','88572806','1542865822','admin','1543049045','119.248.2.104','3');
INSERT INTO `destoon_validate` VALUES('186','东莞市美贝仕铜材处理剂开发有限公司','company','https://www.savt.cn/file/upload/201811/23/141625231506.jpg','','','dgmbs10','1542953791','admin','1543049045','120.234.134.126','3');
INSERT INTO `destoon_validate` VALUES('187','南通诚鼎网络科技有限公司','company','https://www.savt.cn/file/upload/201811/23/154040191508.jpg','','','qq3096281066','1542958851','admin','1543049045','106.6.90.138','3');
INSERT INTO `destoon_validate` VALUES('188','马嘉晨','truename','https://www.savt.cn/file/upload/201811/23/154310451508.png','','','qq3096281066','1542959000','admin','1543049034','106.6.90.138','3');
INSERT INTO `destoon_validate` VALUES('189','杨丽红','truename','https://www.savt.cn/file/upload/201811/24/040305541510.jpg','','','cxpaper','1543003390','admin','1543049034','58.44.133.236','3');
INSERT INTO `destoon_validate` VALUES('190','627101533@qq.com','email','','','','cxpaper','1543003431','system','1543003431','58.44.133.236','3');
INSERT INTO `destoon_validate` VALUES('191','西安骏霖环艺广告策划有限公司','company','https://www.savt.cn/file/upload/201811/24/172915861513.png','','','jl15129249961','1543051772','admin','1543322710','113.140.250.33','3');
INSERT INTO `destoon_validate` VALUES('192','黄孟平','truename','https://www.savt.cn/file/upload/201811/24/173901701513.jpg','','','jl15129249961','1543052353','admin','1543232314','113.140.250.33','3');
INSERT INTO `destoon_validate` VALUES('193','杨凌金山农业科技有限责任公司','company','https://www.savt.cn/file/upload/201811/26/154658471518.jpg','','','yanglingjinshan','1543218428','admin','1543322710','1.80.84.190','3');
INSERT INTO `destoon_validate` VALUES('194','固安县千陆过滤科技有限公司','company','https://www.savt.cn/file/upload/201811/27/090841761523.jpg','','','qianlu688','1543280932','admin','1543322710','110.251.223.224','3');
INSERT INTO `destoon_validate` VALUES('195','山东易科安林环境科技有限公司','company','https://www.savt.cn/file/upload/201811/27/113209701524.jpg','','','ecoany2','1543289540','admin','1543322710','123.131.107.105','3');
INSERT INTO `destoon_validate` VALUES('196','霸州市斯诺特过滤净化设备有限公司','company','https://www.savt.cn/file/upload/201811/27/12295117793.jpg','','','sinuote','1543292996','admin','1543322710','110.251.208.56','3');
INSERT INTO `destoon_validate` VALUES('197','叶建城','truename','https://www.savt.cn/file/upload/201811/27/143919971526.jpg','https://www.savt.cn/file/upload/201811/27/143925821526.jpg','','e18760949124','1543300777','admin','1543322701','116.54.21.161','3');
INSERT INTO `destoon_validate` VALUES('198','云南世伟洛克阀门有限公司','company','https://www.savt.cn/file/upload/201811/27/143951181526.jpg','','','e18760949124','1543300801','admin','1543322710','116.54.21.161','3');
INSERT INTO `destoon_validate` VALUES('199','武汉过控科技有限公司','company','https://www.savt.cn/file/upload/201811/28/080818851341.jpg','','','whguokong','1543363708','admin','1543580258','171.43.143.37','3');
INSERT INTO `destoon_validate` VALUES('200','王新中','truename','https://www.savt.cn/file/upload/201811/29/160046491545.jpg','https://www.savt.cn/file/upload/201811/29/160055821545.jpg','','tpsled','1543478463','admin','1543580249','113.110.152.123','3');
INSERT INTO `destoon_validate` VALUES('201','深圳市拓普绿色科技有限公司','company','https://www.savt.cn/file/upload/201811/29/160120251545.jpg','https://www.savt.cn/file/upload/201811/29/160127661545.jpg','https://www.savt.cn/file/upload/201811/29/160134691545.jpg','tpsled','1543478509','admin','1543580258','113.110.152.123','3');
INSERT INTO `destoon_validate` VALUES('202','13528719690@163.com','email','','','','tpsled','1543478585','system','1543478585','113.110.152.123','3');
INSERT INTO `destoon_validate` VALUES('203','13822236817@163.com','email','','','','qbrzzxzx','1543559280','system','1543559280','120.230.80.12','3');
INSERT INTO `destoon_validate` VALUES('204','梁卫健','truename','https://www.savt.cn/file/upload/201811/30/142821501552.jpg','https://www.savt.cn/file/upload/201811/30/142828761552.jpg','https://www.savt.cn/file/upload/201811/30/142836381552.jpg','qbrzzxzx','1543559324','admin','1543580249','120.230.80.12','3');
INSERT INTO `destoon_validate` VALUES('205','企标（广州）认证咨询服务有限公司','company','https://www.savt.cn/file/upload/201811/30/142911551552.jpg','','','qbrzzxzx','1543559358','admin','1543580258','120.230.80.12','3');
INSERT INTO `destoon_validate` VALUES('206','曹华星','truename','https://www.savt.cn/file/upload/201812/04/161003771572.jpg','','','qq7917565','1543911016','admin','1543922491','180.102.123.45','3');
INSERT INTO `destoon_validate` VALUES('207','南京星坤环保科技有限公司','company','https://www.savt.cn/file/upload/201812/04/161027781572.jpg','','','qq7917565','1543911032','admin','1543922507','180.102.123.45','3');
INSERT INTO `destoon_validate` VALUES('208','朴春学','truename','https://www.savt.cn/file/upload/201812/08/110420901584.jpg','','','lblmy','1544238273','admin','1544428135','183.54.193.119','3');
INSERT INTO `destoon_validate` VALUES('209','广州利葆乐贸易有限公司','company','https://www.savt.cn/file/upload/201812/08/110452821584.jpg','','','lblmy','1544238299','admin','1544428128','183.54.193.119','3');
INSERT INTO `destoon_validate` VALUES('210','737392387@qq.com','email','','','','cjjcjhcjp','1544412392','validate','1544412392','183.48.20.216','3');
INSERT INTO `destoon_validate` VALUES('211','15918889422@qq.com','email','','','','cjjcjhcjp','1544413465','send','1544413465','183.48.20.216','3');

DROP TABLE IF EXISTS `destoon_video_14`;
CREATE TABLE `destoon_video_14` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `fee` float NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL DEFAULT '',
  `album` varchar(100) NOT NULL,
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `pptword` varchar(255) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `video` varchar(255) NOT NULL DEFAULT '',
  `mobile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `introduce` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`),
  KEY `username` (`username`),
  KEY `addtime` (`addtime`),
  KEY `catid` (`catid`),
  KEY `album` (`album`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='视频';


DROP TABLE IF EXISTS `destoon_video_data_14`;
CREATE TABLE `destoon_video_data_14` (
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='视频内容';


DROP TABLE IF EXISTS `destoon_vote`;
CREATE TABLE `destoon_vote` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` int(10) unsigned NOT NULL DEFAULT '0',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `groupid` varchar(255) NOT NULL,
  `verify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `choose` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `vote_min` smallint(2) unsigned NOT NULL DEFAULT '0',
  `vote_max` smallint(2) unsigned NOT NULL DEFAULT '0',
  `votes` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `fromtime` int(10) unsigned NOT NULL DEFAULT '0',
  `totime` int(10) unsigned NOT NULL DEFAULT '0',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `linkto` varchar(255) NOT NULL DEFAULT '',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `template_vote` varchar(30) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  `s1` varchar(255) NOT NULL DEFAULT '',
  `s2` varchar(255) NOT NULL DEFAULT '',
  `s3` varchar(255) NOT NULL DEFAULT '',
  `s4` varchar(255) NOT NULL DEFAULT '',
  `s5` varchar(255) NOT NULL DEFAULT '',
  `s6` varchar(255) NOT NULL DEFAULT '',
  `s7` varchar(255) NOT NULL DEFAULT '',
  `s8` varchar(255) NOT NULL DEFAULT '',
  `s9` varchar(255) NOT NULL DEFAULT '',
  `s10` varchar(255) NOT NULL DEFAULT '',
  `v1` int(10) unsigned NOT NULL DEFAULT '0',
  `v2` int(10) unsigned NOT NULL DEFAULT '0',
  `v3` int(10) unsigned NOT NULL DEFAULT '0',
  `v4` int(10) unsigned NOT NULL DEFAULT '0',
  `v5` int(10) unsigned NOT NULL DEFAULT '0',
  `v6` int(10) unsigned NOT NULL DEFAULT '0',
  `v7` int(10) unsigned NOT NULL DEFAULT '0',
  `v8` int(10) unsigned NOT NULL DEFAULT '0',
  `v9` int(10) unsigned NOT NULL DEFAULT '0',
  `v10` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='投票';


DROP TABLE IF EXISTS `destoon_vote_record`;
CREATE TABLE `destoon_vote_record` (
  `rid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `username` varchar(30) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  `votetime` int(10) unsigned NOT NULL DEFAULT '0',
  `votes` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`rid`),
  KEY `itemid` (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='投票记录';


DROP TABLE IF EXISTS `destoon_webpage`;
CREATE TABLE `destoon_webpage` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(30) NOT NULL DEFAULT '',
  `areaid` int(10) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `style` varchar(50) NOT NULL DEFAULT '',
  `content` mediumtext NOT NULL,
  `seo_title` varchar(255) NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) NOT NULL DEFAULT '',
  `seo_description` varchar(255) NOT NULL DEFAULT '',
  `editor` varchar(30) NOT NULL DEFAULT '',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(4) NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `islink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `linkurl` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='单网页';

INSERT INTO `destoon_webpage` VALUES('1','1','0','0','关于我们','','&nbsp;&nbsp;&nbsp; 【商途网】<a href=\"https://www.savt.cn\">www.savt.cn</a>---国内最领先的一站式B2B电子商务服务平台，提供全面的B2B电商行业资讯、供应、求购、展会、品牌、招商、资讯等信息，是中国中小企业销售产品、拓展市场的最佳电子商务平台！佰企网专注中小型企业免费产品供求信息发布，免费网络店铺搭建，免费企业品牌展示，免费电子商务运营策划，力求让国内所有企业都走上电子商务之路。<br />\r\n&nbsp; &nbsp; &nbsp;商途网立志成为全球一流的企业电商服务提供商，为企业提供最全面、最准确、最有力的商务信息支持。同时将凭借雄厚的技术实力和丰富的开发经验，融合领先的理念和敏锐的市场意识，为各行业企业提供从互联网营销到企业信息门户的一体化服务。以一流的技术、一流的管理为客户提供一流的服务。','','','','admin','1535077284','4','77','0','about/index.html','','');
INSERT INTO `destoon_webpage` VALUES('2','1','0','0','联系方式','','企业邮箱：wzgwzg# qq.com<br />\r\n客服QQ：3821369<br />\r\n24小时服务电话：0536-3319199','','','','admin','1535077310','3','63','0','about/contact.html','','');
INSERT INTO `destoon_webpage` VALUES('3','1','0','0','使用协议','','<div>欢迎阅读商途网服务条款协议(下称&ldquo;本协议&rdquo;)，您应当在使用服务之前认真阅读本协议全部内容，且对本协议中加粗字体显示的内容，商途网督促您应重点阅读。本协议阐述之条款和条件适用于您使用商途网，所提供的在全球企业间(B-TO-B)电子市场(e-market)中进行贸易和交流的各种工具和服务(下称&ldquo;服务&rdquo;)。</div>\r\n<div>&nbsp;</div>\r\n<div>1. 接受条款。&nbsp;</div>\r\n<div>以任何方式进入并使用商途网服务即表示您已充分阅读、理解并同意自己已经与商途网订立本协议，且您将受本协议的条款和条件(&ldquo;条款&rdquo;) 约束。商途网可随时自行全权决定更改&ldquo;条款&rdquo;。如&ldquo;条款&rdquo;有任何变更，商途网仅将在网站上发布新条款予以公示，不再单独通知予您。如您不同意相关变更，则必须停止使用&ldquo;服务&rdquo;。经修订的&ldquo;条款&rdquo;一经在商途网网站公布后，立即自动生效。一旦您继续使用&ldquo;服务&rdquo;，则表示您已接受经修订的&ldquo;条款&rdquo;，当您与商途网发生争议时，应以最新的&ldquo;条款&rdquo;为准。除另行明确声明外，任何使&ldquo;服务&rdquo;范围扩大或功能增强的新内容均受本协议约束。</div>\r\n<div>本协议内容包括协议正文及所有商途网已经发布或将来可能发布的各类规则。所有规则为本协议不可分割的一部分，与本协议正文具有同等法律效力。</div>\r\n<div>&nbsp;</div>\r\n<div>2.服务使用对象。</div>\r\n<div>&ldquo;服务&rdquo;仅供能够根据相关法律订立具有法律约束力的合约的自然人、法人或其他组织使用。因此，您的年龄必须在十八周岁或以上，才可使用商途网服务。如不符合本项条件，请勿使用&ldquo;服务&rdquo;。商途网可随时自行全权决定拒绝向任何服务对象提供&ldquo;服务&rdquo;。&ldquo;服务&rdquo;不会提供给被暂时或永久中止资格的商途网会员。</div>\r\n<div>&nbsp;</div>\r\n<div>3. 收费。&nbsp;</div>\r\n<div>商途网保留在根据第1条通知您后，收取&ldquo;服务&rdquo;费用的权利。您因进行交易、向商途网获取有偿服务或接触商途网服务器而发生的所有应纳税赋，以及相关硬件、软件、通讯、网络服务及其他方面的费用均由您自行承担。商途网保留在无须发出书面通知，仅在商途网网站公示的情况下，暂时或永久地更改或停止部分或全部&ldquo;服务&rdquo;的权利。</div>\r\n<div>&nbsp;</div>\r\n<div>4.商途网网站仅作为信息发布或交易地点。&nbsp;</div>\r\n<div>您完全了解商途网网站上的信息系用户自行发布，且可能存在风险和瑕疵。商途网网站仅作为用户物色交易对象，就货物和服务的交易进行协商，以及获取各类与贸易相关的服务的地点。但是，商途网不能控制交易所涉及的物品的质量、安全或合法性，商贸信息的真实性或准确性，以及交易方履行其在贸易协议项下的各项义务的能力。商途网不能也不会控制交易各方能否履行协议义务。此外，您应注意到，与外国国民、未成年人或以欺诈手段行事的人进行交易的风险是客观存在的。您应自行谨慎判断确定相关物品及/或信息的真实性、合法性和有效性，并自行承担因此产生的责任与损失。</div>\r\n<div>&nbsp;</div>\r\n<div>5.您的资料和供买卖的物品。&nbsp;</div>\r\n<div>&ldquo;您的资料&rdquo;包括您在注册、交易或列举物品过程中、在任何公开信息场合或通过任何电子邮件形式，向商途网或其他用户提供的任何资料，包括数据、文本、软件、音乐、声响、照片、图画、影像、词句或其他材料。您应对&ldquo;您的资料&rdquo;负全部责任，而商途网仅作为您在网上发布和刊登&ldquo;您的资料&rdquo;的被动渠道。但是，倘若商途网认为&ldquo;您的资料&rdquo;可能使商途网承担任何法律或道义上的责任，或可能使商途网 (全部或部分地) 失去商途网的互联网服务供应商或其他供应商的服务，或您未在商途网规定的期限内登录或再次登录网站，则商途网可自行全权决定对&ldquo;您的资料&rdquo;采取商途网认为必要或适当的任何行动，包括但不限于删除该类资料。您特此保证，您对提交给商途网的&ldquo;您的资料&rdquo;拥有全部权利，包括全部版权。您确认，商途网没有责任去认定或决定您提交给商途网的资料哪些是应当受到保护的，对享有&ldquo;服务&rdquo;的其他用户使用&ldquo;您的资料&rdquo;，商途网也不必负责。</div>\r\n<div>&nbsp;</div>\r\n<div>5.1 注册义务。&nbsp;</div>\r\n<div>如您在商途网网站注册，您同意：(a) 根据商途网网站刊载的会员资料表格的要求，提供关于您或贵公司的真实、准确、完整和反映当前情况的资料；(b) 维持并及时更新会员资料，使其保持真实、准确、完整和反映当前情况。倘若您提供任何不真实、不准确、不完整或不能反映当前情况的资料，或商途网有合理理由怀疑该等资料不真实、不准确、不完整或不能反映当前情况，商途网有权暂停或终止您的注册身份及资料，并拒绝您在目前或将来对&ldquo;服务&rdquo;(或其任何部份) 以任何形式使用。如您代表一家公司或其他法律主体在商途网登记，则您声明和保证，您有权使该公司或其他法律主体受本协议&ldquo;条款&rdquo;约束。</div>\r\n<div>&nbsp;</div>\r\n<div>5.2 会员注册名、密码和保密。&nbsp;</div>\r\n<div>在您按照注册页面提示填写信息、阅读并同意本协议并完成全部注册程序后或以其他商途网允许的方式实际使用商途网网站服务时，您即成为商途网会员（亦称会员），商途网根据会员注册名和密码确认您的身份。您须自行负责对您的会员注册名和密码保密，且须对您在会员注册名和密码下发生的所有活动（包括但不限于发布信息资料、网上点击同意或提交各类规则协议、网上续签协议或购买服务等）承担责任。您同意：(a) 如发现任何人未经授权使用您的会员注册名或密码，或发生违反保密规定的任何其他情况，您会立即通知商途网；及 (b) 确保您在每个上网时段结束时，以正确步骤离开网站。商途网不能也不会对因您未能遵守本款规定而发生的任何损失或损毁负责。您理解商途网对您的请求采取行动需要合理时间，商途网对在采取行动前已经产生的后果（包括但不限于您的任何损失）不承担任何责任。</div>\r\n<div>&nbsp;</div>\r\n<div>5.3 关于您的资料的规则。&nbsp;</div>\r\n<div>您同意，&ldquo;您的资料&rdquo;和您发布在商途网网站上的任何&ldquo;物品&rdquo;（泛指一切可供依法交易的、有形的或无形的、以各种形态存在的某种具体的物品，或某种权利或利益，或某种票据或证券，或某种服务或行为。本协议中&ldquo;物品&rdquo;一词均含此义）</div>\r\n<div>a. 不会有欺诈成份，与售卖伪造或盗窃无涉；&nbsp;</div>\r\n<div>b. 不会侵犯任何第三者对该物品享有的物权，或版权、专利、商标、商业秘密或其他知识产权，或隐私权、名誉权等任何权利；</div>\r\n<div>c. 不会违反任何法律、法规、条例或规章 (包括但不限于关于规范出口管理、贸易配额、保护消费者、不正当竞争或虚假广告的法律、法规、条例或规章)；</div>\r\n<div>d. 不会含有诽谤（包括商业诽谤）、恐吓或骚扰的内容；</div>\r\n<div>e. 不会含有淫秽、或包含任何儿童色情内容；&nbsp;</div>\r\n<div>f. 不会含有蓄意毁坏、恶意干扰、秘密地截取或侵占任何系统、数据或个人资料的任何病毒、伪装破坏程序、电脑蠕虫、定时程序炸弹或其他电脑程序；&nbsp;</div>\r\n<div>g. 不会直接或间接与下述各项货物或服务连接，或包含对下述各项货物或服务的描述：(i) 本协议项下禁止的货物或服务；或 (ii) 您无权连接或包含的货物或服务。此外，您同意不会：(h) 在与任何连锁信件、大量胡乱邮寄的电子邮件、滥发电子邮件或任何复制或多余的信息有关的方面使用&ldquo;服务&rdquo;；(i) 未经其他人士同意，利用&ldquo;服务&rdquo;收集其他人士的电子邮件地址及其他资料；或 (j) 利用&ldquo;服务&rdquo;制作虚假的电子邮件地址，或以其他形式试图在发送人的身份或信息的来源方面误导其他人士。</div>\r\n<div>&nbsp;</div>\r\n<div>5.4 被禁止物品。&nbsp;</div>\r\n<div>您不得在商途网网站公布或通过商途网网站买卖：(a) 可能使商途网违反任何相关法律、法规、条例或规章的任何物品；或 (b) 商途网认为应禁止或不适合通过本网站买卖的任何物品。</div>\r\n<div>&nbsp;</div>\r\n<div>6. 您完全理解并同意不可撤销地授予商途网及其关联公司下列权利：</div>\r\n<div>6.1对于您提供的资料，您授予商途网及其关联公司独家的、全球通用的、永久的、免费的许可使用权利 (并有权在多个层面对该权利进行再授权)，使商途网及其关联公司有权(全部或部份地) 使用、复制、修订、改写、发布、翻译、分发、执行和展示&quot;您的资料&quot;或制作其派生作品，和/或以现在已知或日后开发的任何形式、媒体或技术，将&quot;您的资料&quot;纳入其他作品内。</div>\r\n<div>6.2当您违反本协议或与商途网签订的其他协议的约定，商途网有权以任何方式通知关联公司（包括但不限于支付宝、淘宝、阿里金融、阿里云，中国雅虎等，下同），要求其对您的权益采取限制措施包括但不限于要求支付宝公司将您帐户内的款项支付给商途网指定的对象，要求关联公司中止、终止对您提供部分或全部服务，且在其经营或实际控制的任何网站公示您的违约情况。</div>\r\n<div>6.3同样，当您向商途网关联公司作出任何形式的承诺，且相关公司已确认您违反了该承诺，则商途网有权立即按您的承诺约定的方式对您的账户采取限制措施，包括但不限于中止或终止向您提供服务，并公示相关公司确认的您的违约情况。您了解并同意，商途网无须就相关确认与您核对事实，或另行征得您的同意，且商途网无须就此限制措施或公示行为向您承担任何的责任。</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>7.隐私。&nbsp;</div>\r\n<div>尽管有第6条所规定的许可使用权，但基于保护您的隐私是商途网的一项基本原则，为此商途网还将根据商途网的隐私声明使用&quot;您的资料&quot;。商途网隐私声明的全部条款属于本协议的一部份，因此，您必须仔细阅读。请注意，您一旦自愿地在商途网交易地点披露&quot;您的资料&quot;，该等资料即可能被其他人士获取和使用。</div>\r\n<div>&nbsp;</div>\r\n<div>8.信息发布及交易程序。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>8.1 添加产品描述条目。产品描述是由您提供的在商途网网站上展示的文字描述、图画和/或照片，可以是(a) 对您拥有而您希望出售的产品的描述；或 (b) 对您正寻找的产品的描述。您可在商途网网站发布任一类产品描述，或两种类型同时发布，条件是，您必须将该等产品描述归入正确的类目内。商途网不对产品描述的准确性或内容负责。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>8.2 就交易进行协商。&nbsp;</div>\r\n<div>交易各方通过在商途网网站上明确描述报盘和回盘，进行相互协商。所有各方接纳报盘或回盘将使所涉及的商途网用户有义务完成交易。除非在特殊情况下，诸如用户在您提出报盘后实质性地更改对物品的描述或澄清任何文字输入错误，或您未能证实交易所涉及的用户的身份等，报盘和承诺均不得撤回。</div>\r\n<div>&nbsp;</div>\r\n<div>8.3 处理交易争议。&nbsp;</div>\r\n<div>(i)商途网不涉及用户间因交易而产生的法律关系及法律纠纷，不会且不能牵涉进交易各方的交易当中。倘若您与一名或一名以上用户，或与您通过商途网网站获取其服务的第三者服务供应商发生争议，您免除商途网 (及商途网代理人和雇员) 在因该等争议而引起的，或在任何方面与该等争议有关的不同种类和性质的任何(实际和后果性的) 权利主张、要求和损害赔偿等方面的责任。</div>\r\n<div>(ii)商途网有权受理并调处您与其他用户因交易产生的争议，同时有权单方面独立判断其他用户对您的投诉及(或)索偿是否成立，若商途网判断索偿成立，则您应及时使用自有资金进行偿付，否则商途网有权使用您交纳的保证金（如有）或扣减已购商途网及其关联公司的产品或服务中未履行部分的费用的相应金额或您在商途网网站所有账户下的其他资金(或权益)等进行相应偿付。商途网没有使用自用资金进行偿付的义务，但若进行了该等支付，您应及时赔偿商途网的全部损失，否则商途网有权通过前述方式抵减相应资金或权益，如仍无法弥补商途网损失，则商途网保留继续追偿的权利。因商途网非司法机构，您完全理解并承认，商途网对证据的鉴别能力及对纠纷的处理能力有限，受理贸易争议完全是基于您之委托，不保证争议处理结果符合您的期望，亦不对争议处理结果承担任何责任。商途网有权决定是否参与争议的调处。</div>\r\n<div>(iii) 商途网有权通过电子邮件等联系方式向您了解情况，并将所了解的情况通过电子邮件等方式通知对方，您有义务配合商途网的工作，否则商途网有权做出对您不利的处理结果。</div>\r\n<div>(ⅳ)经生效法律文书确认用户存在违法或违反本协议行为或者商途网自行判断用户涉嫌存在违法或违反本协议行为的，商途网有权在商途网中文网站上以网络发布形式公布用户的违法行为并做出处罚（包括但不限于限权、终止服务等）。</div>\r\n<div>&nbsp;</div>\r\n<div>8.4 运用常识。&nbsp;</div>\r\n<div>商途网不能亦不试图对其他用户通过&ldquo;服务&rdquo;提供的资料进行控制。就其本质而言，其他用户的资料，可能是令人反感的、有害的或不准确的，且在某些情况下可能带有错误的标识说明或以欺诈方式加上标识说明。商途网希望您在使用商途网网站时，小心谨慎并运用常识。</div>\r\n<div>&nbsp;</div>\r\n<div>9.交易系统。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>9.1 不得操纵交易。&nbsp;</div>\r\n<div>您同意不利用帮助实现蒙蔽或欺骗意图的同伙(下属的客户或第三方)，操纵与另一交易方所进行的商业谈判的结果。</div>\r\n<div>&nbsp;</div>\r\n<div>9.2 系统完整性。&nbsp;</div>\r\n<div>您同意，您不得使用任何装置、软件或例行程序干预或试图干预商途网中文网站的正常运作或正在商途网网站上进行的任何交易。您不得采取对任何将不合理或不合比例的庞大负载加诸商途网网络结构的行动。您不得向任何第三者披露您的密码，或与任何第三者共用您的密码，或为任何未经批准的目的使用您的密码。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>9.3 反馈。&nbsp;</div>\r\n<div>您不得采取任何可能损害信息反馈系统的完整性的行动，诸如：利用第二会员身份标识或第三者为您本身留下正面反馈；利用第二会员身份标识或第三者为其他用户留下负面反馈 (反馈数据轰炸)；或在用户未能履行交易范围以外的某些行动时，留下负面的反馈 (反馈恶意强加)。</div>\r\n<div>&nbsp;</div>\r\n<div>9.4 不作商业性利用。&nbsp;</div>\r\n<div>您同意，您不得对任何资料作商业性利用，包括但不限于在未经商途网授权高层管理人员事先书面批准的情况下，复制在商途网网站上展示的任何资料并用于商业用途。</div>\r\n<div>&nbsp;</div>\r\n<div>10. 终止或访问限制。&nbsp;</div>\r\n<div>您同意，在商途网未向您收费的情况下，商途网可自行全权决定以任何理由 (包括但不限于商途网认为您已违反本协议的字面意义和精神，或您以不符合本协议的字面意义和精神的方式行事，或您在超过90天的时间内未以您的帐号及密码登录网站) 终止您的&ldquo;服务&rdquo;密码、帐户 (或其任何部份) 或您对&ldquo;服务&rdquo;的使用，并删除和丢弃您在使用&ldquo;服务&rdquo;中提交的 &ldquo;您的资料&rdquo;。您同意，在商途网向您收费的情况下，商途网应基于合理的怀疑且经电子邮件通知的情况下实施上述终止服务的行为。商途网同时可自行全权决定，在发出通知或不发出通知的情况下，随时停止提供&ldquo;服务&rdquo;或其任何部份。您同意，根据本协议的任何规定终止您使用&ldquo;服务&rdquo;之措施可在不发出事先通知的情况下实施，并承认和同意，商途网可立即使您的帐户无效，或撤销您的帐户以及在您的帐户内的所有相关资料和档案，和/或禁止您进一步接入该等档案或&ldquo;服务&rdquo;。帐号终止后，商途网没有义务为您保留原帐号中或与之相关的任何信息，或转发任何未曾阅读或发送的信息给您或第三方。此外，您同意，商途网不会就终止您接入&ldquo;服务&rdquo;而对您或任何第三者承担任何责任。第12、13、14和22各条应在本协议终止后继续有效。</div>\r\n<div>&nbsp;</div>\r\n<div>11. 违反规则会有什么后果？&nbsp;</div>\r\n<div>在不限制其他补救措施的前提下，发生下述任一情况，商途网可立即发出警告，暂时中止、永久中止或终止您的会员资格，删除您的任何现有产品信息，以及您在网站上展示的任何其他资料：(i) 您违反本协议；(ii) 商途网无法核实或鉴定您向商途网提供的任何资料；或 (iii) 商途网相信您的行为可能会使您、商途网用户或通过商途网或商途网网站提供服务的第三者服务供应商发生任何法律责任。在不限制任何其他补救措施的前提下，倘若发现您从事涉及商途网网站的诈骗活动，商途网可暂停或终止您的帐户。</div>\r\n<div>&nbsp;</div>\r\n<div>12. 服务&ldquo;按现状&rdquo;提供。&nbsp;</div>\r\n<div>商途网会尽一切努力使您在使用商途网网站的过程中得到乐趣。遗憾的是，商途网不能随时预见到任何技术上的问题或其他困难。该等困难可能会导致数据损失或其他服务中断。为此，您明确理解和同意，您使用&ldquo;服务&rdquo;的风险由您自行承担。&ldquo;服务&rdquo;以&ldquo;按现状&rdquo;和&ldquo;按可得到&rdquo;的基础提供。商途网明确声明不作出任何种类的所有明示或暗示的保证，包括但不限于关于适销性、适用于某一特定用途和无侵权行为等方面的保证。商途网对下述内容不作保证：(i)&ldquo;服务&rdquo;会符合您的要求；(ii)&ldquo;服务&rdquo;不会中断，且适时、安全和不带任何错误；(iii) 通过使用&ldquo;服务&rdquo;而可能获取的结果将是准确或可信赖的；及 (iv) 您通过&ldquo;服务&rdquo;而购买或获取的任何产品、服务、资料或其他材料的质量将符合您的预期。通过使用&ldquo;服务&rdquo;而下载或以其他形式获取任何材料是由您自行全权决定进行的，且与此有关的风险由您自行承担，对于因您下载任何该等材料而发生的您的电脑系统的任何损毁或任何数据损失，您将自行承担责任。您从商途网或通过或从&ldquo;服务&rdquo;获取的任何口头或书面意见或资料，均不产生未在本协议内明确载明的任何保证。</div>\r\n<div>&nbsp;</div>\r\n<div>13. 责任范围。&nbsp;</div>\r\n<div>您明确理解和同意，商途网不对因下述任一情况而发生的任何损害赔偿承担责任，包括但不限于利润、商誉、使用、数据等方面的损失或其他无形损失的损害赔偿 (无论商途网是否已被告知该等损害赔偿的可能性)：(i) 使用或未能使用&ldquo;服务&rdquo;；(ii) 因通过或从&ldquo;服务&rdquo;购买或获取任何货物、样品、数据、资料或服务，或通过或从&ldquo;服务&rdquo;接收任何信息或缔结任何交易所产生的获取替代货物和服务的费用；(iii) 未经批准接入或更改您的传输资料或数据；(iv) 任何第三者对&ldquo;服务&rdquo;的声明或关于&ldquo;服务&rdquo;的行为；或 (v) 因任何原因而引起的与&ldquo;服务&rdquo;有关的任何其他事宜，包括疏忽。</div>\r\n<div>&nbsp;</div>\r\n<div>14. 赔偿。&nbsp;</div>\r\n<div>您同意，因您违反本协议或经在此提及而纳入本协议的其他文件，或因您违反了法律或侵害了第三方的权利，而使第三方对商途网及其子公司、分公司、董事、职员、代理人提出索赔要求（包括司法费用和其他专业人士的费用），您必须赔偿给商途网及其子公司、分公司、董事、职员、代理人，使其等免遭损失。</div>\r\n<div>&nbsp;</div>\r\n<div>15. 遵守法律。&nbsp;</div>\r\n<div>您应遵守与您使用&ldquo;服务&rdquo;，以及与您竞买、购买和销售任何物品以及提供商贸信息有关的所有相关的法律、法规、条例和规章。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>16. 无代理关系。&nbsp;</div>\r\n<div>您与商途网仅为独立订约人关系。本协议无意结成或创设任何代理、合伙、合营、雇用与被雇用或特许权授予与被授予关系。</div>\r\n<div>&nbsp;</div>\r\n<div>17. 广告和金融服务。&nbsp;</div>\r\n<div>您与在&ldquo;服务&rdquo;上或通过&ldquo;服务&rdquo;物色的刊登广告人士通讯或进行业务往来或参与其推广活动，包括就相关货物或服务付款和交付相关货物或服务，以及与该等业务往来相关的任何其他条款、条件、保证或声明，仅限于在您和该刊登广告人士之间发生。您同意，对于因任何该等业务往来或因在&ldquo;服务&rdquo;上出现该等刊登广告人士而发生的任何种类的任何损失或损毁，商途网无需负责或承担任何责任。您如打算通过&ldquo;服务&rdquo;创设或参与与任何公司、股票行情、投资或证券有关的任何服务，或通过&ldquo;服务&rdquo;收取或要求与任何公司、股票行情、投资或证券有关的任何新闻信息、警戒性信息或其他资料，敬请注意，商途网不会就通过&ldquo;服务&rdquo;传送的任何该等资料的准确性、有用性或可用性、可获利性负责或承担任何责任，且不会对根据该等资料而作出的任何交易或投资决策负责或承担任何责任。&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>18. 链接。&nbsp;</div>\r\n<div>&ldquo;服务&rdquo;或第三者均可提供与其他万维网网站或资源的链接。由于商途网并不控制该等网站和资源，您承认并同意，商途网并不对该等外在网站或资源的可用性负责，且不认可该等网站或资源上或可从该等网站或资源获取的任何内容、宣传、产品、服务或其他材料，也不对其等负责或承担任何责任。您进一步承认和同意，对于任何因使用或信赖从此类网站或资源上获取的此类内容、宣传、产品、服务或其他材料而造成（或声称造成）的任何直接或间接损失，商途网均不承担责任。</div>\r\n<div>&nbsp;</div>\r\n<div>19. 通知。&nbsp;</div>\r\n<div>除非另有明确规定，任何通知应以电子邮件形式发送，(就商途网而言) 电子邮件地址为 qslb2b@163.com，或 (就您而言) 发送到您在登记过程中向商途网提供的电子邮件地址，或有关方指明的该等其他地址。在电子邮件发出二十四 (24) 小时后，通知应被视为已送达，除非发送人被告知相关电子邮件地址已作废。或者，商途网可通过邮资预付挂号邮件并要求回执的方式，将通知发到您在登记过程中向商途网提供的地址。在该情况下，在付邮当日三 (3) 天后通知被视为已送达。</div>\r\n<div>&nbsp;</div>\r\n<div>20. 不可抗力。&nbsp;</div>\r\n<div>对于因商途网合理控制范围以外的原因，包括但不限于自然灾害、罢工或骚乱、物质短缺或定量配给、暴动、战争行为、政府行为、通讯或其他设施故障或严重伤亡事故等，致使商途网延迟或未能履约的，商途网不对您承担任何责任。</div>\r\n<div>&nbsp;</div>\r\n<div>21. 转让。&nbsp;</div>\r\n<div>商途网转让本协议无需经您同意。</div>\r\n<div>&nbsp;</div>\r\n<div>22. 其他规定。&nbsp;</div>\r\n<div>本协议取代您和商途网先前就相同事项订立的任何书面或口头协议。本协议各方面应受中华人民共和国大陆地区法律的管辖。倘若本协议任何规定被裁定为无效或不可强制执行，该项规定应被撤销，而其余规定应予执行。条款标题仅为方便参阅而设，并不以任何方式界定、限制、解释或描述该条款的范围或限度。商途网未就您或其他人士的某项违约行为采取行动，并不表明商途网撤回就任何继后或类似的违约事件采取行动的权利。</div>\r\n<div>23. 诉讼。&nbsp;</div>\r\n<div>因本协议或商途网服务所引起或与其有关的任何争议应向山东省聊城市人民法院提起诉讼，并以中华人民共和国法律为管辖法律。</div>\r\n<div>&nbsp;</div>','','','','admin','1543723089','2','35','0','about/agreement.html','','');

DROP TABLE IF EXISTS `destoon_webs`;
CREATE TABLE `destoon_webs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `web` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `destoon_webs` VALUES('1','http://www.nengyuan114.com','新能源商务网','0');

DROP TABLE IF EXISTS `destoon_webs_flow`;
CREATE TABLE `destoon_webs_flow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `web_sell_id` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT NULL,
  `web_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `destoon_weixin_auto`;
CREATE TABLE `destoon_weixin_auto` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `reply` text NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信回复';


DROP TABLE IF EXISTS `destoon_weixin_bind`;
CREATE TABLE `destoon_weixin_bind` (
  `username` varchar(30) NOT NULL DEFAULT '',
  `sid` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信扫码绑定';


DROP TABLE IF EXISTS `destoon_weixin_chat`;
CREATE TABLE `destoon_weixin_chat` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `editor` varchar(30) NOT NULL,
  `openid` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL,
  `event` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `misc` mediumtext NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `openid` (`openid`),
  KEY `addtime` (`addtime`),
  KEY `event` (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信消息';


DROP TABLE IF EXISTS `destoon_weixin_user`;
CREATE TABLE `destoon_weixin_user` (
  `itemid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `openid` varchar(255) NOT NULL DEFAULT '',
  `nickname` varchar(255) NOT NULL DEFAULT '',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `headimgurl` varchar(255) NOT NULL,
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `visittime` int(10) unsigned NOT NULL DEFAULT '0',
  `credittime` int(10) unsigned NOT NULL DEFAULT '0',
  `subscribe` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `push` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`itemid`),
  UNIQUE KEY `openid` (`openid`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信用户';


