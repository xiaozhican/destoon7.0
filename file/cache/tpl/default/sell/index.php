<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header');?>
<script src="<?php echo DT_SKIN;?>js/base.js"></script>
<div class="m">
<div class="nav"><a href="<?php echo $MODULE['1']['linkurl'];?>">首页</a> <i>&gt;</i> <a href="<?php echo $MOD['linkurl'];?>"><?php echo $MOD['name'];?></a> <i>&gt;</i> <?php echo cat_pos($CAT, ' <i>&gt;</i> ');?></div>
</div>
<div class="w1200 selectList">
        <dl>
            <dt>行业：</dt>
           <dd class="showCon"> 
      <?php if(is_array($maincat)) { foreach($maincat as $k => $v) { ?>
<a <?php if($v['catid']==$catid) { ?> class="on"<?php } ?>
 href="<?php echo $MOD['linkurl'];?><?php echo $v['linkurl'];?>"><?php echo set_style($v['catname'],$v['style']);?></a>
<?php } } ?>
            </dd>
         <dd class="showMore">
                <h3 class="clearfix"><span>更多</span><i class="iconfont icon-xia1"></i></h3>
            </dd>
        </dl>
        <dl>
            <dt>供货地区：</dt>
          <dd class="showCon"> 
        <?php $mainarea = get_mainarea(0)?>
<?php if(is_array($mainarea)) { foreach($mainarea as $k => $v) { ?>
<a href="<?php echo $MOD['linkurl'];?><?php echo rewrite('search.php?catid='.$catid.'&areaid='.$v['areaid']);?>"><?php echo $v['areaname'];?></a>
<?php } } ?>
            </dd> <dd class="showMore">
                <h3 class="clearfix"><span>更多</span><i class="iconfont icon-xia1"></i></h3>
            </dd>
        </dl>
<dl>
            <dt>类别：</dt>
            <dd> 
       <?php if(is_array($TYPE)) { foreach($TYPE as $k => $v) { ?>
<a href="<?php echo $MOD['linkurl'];?><?php echo rewrite('search.php?catid='.$catid.'&typeid='.$k);?>"><?php echo $v;?></a>
<?php } } ?>
            </dd>
        </dl>
    </div>
<div class="m">

<form method="post">
<div class="sell_tip" id="sell_tip" style="display:none;" title="双击关闭" ondblclick="Dh(this.id);">
<div>
<p>您可以</p>
<input type="submit" value="对比选中" onclick="this.form.action='<?php echo $MOD['linkurl'];?>compare.php';" class="tool-btn"/> 或 
<input type="submit" value="批量询价" onclick="this.form.action='<?php echo $MOD['linkurl'];?>inquiry.php';" class="tool-btn"/>
</div>
</div>
<div class="img_tip" id="img_tip" style="display:none;">&nbsp;</div>
<div class="tool">
<table>
<tr height="30">
<td width="25" align="center"><input type="checkbox" onclick="checkall(this.form);"/></td>
<td>
<input type="submit" value="对比选中" onclick="this.form.action='<?php echo $MOD['linkurl'];?>compare.php';" class="tool-btn"/>&nbsp; &nbsp;
<input type="submit" value="批量询价" onclick="this.form.action='<?php echo $MOD['linkurl'];?>inquiry.php';" class="tool-btn"/>
</td>
<td align="right">
<script type="text/javascript">var sh = '<?php echo $MOD['linkurl'];?>search.php?catid=<?php echo $catid;?>';</script>
<input type="checkbox" onclick="Go(sh+'&price=1');"/>标价&nbsp;
<input type="checkbox" onclick="Go(sh+'&thumb=1');"/>图片&nbsp;
<input type="checkbox" onclick="Go(sh+'&vip=1');"/><?php echo VIP;?>&nbsp;
<select onchange="Go(sh+'&day='+this.value)">
<option value="0">更新时间</option>
<option value="1">1天内</option>
<option value="3">3天内</option>
<option value="7">7天内</option>
<option value="15">15天内</option>
<option value="30">30天内</option>
</select>&nbsp;
<select onchange="Go(sh+'&order='+this.value)">
<option value="0">显示顺序</option>
<option value="2">价格由高到低</option>
<option value="3">价格由低到高</option>
<option value="4"><?php echo VIP;?>级别由高到低</option>
<option value="5"><?php echo VIP;?>级别由低到高</option>
<option value="6">供货量由高到低</option>
<option value="7">供货量由低到高</option>
<option value="8">起订量由高到低</option>
<option value="9">起订量由低到高</option>
</select>&nbsp;
<img src="<?php echo DT_SKIN;?>image/list_img.gif" width="16" height="16" alt="图片列表" align="absmiddle" class="c_p" onclick="Go(sh+'&list=1');"/>&nbsp;
<img src="<?php echo DT_SKIN;?>image/list_mix_on.gif" width="16" height="16" alt="图文列表" align="absmiddle" class="c_p" onclick="Go(sh+'&list=0');"/>&nbsp;
</td>
</tr>
</table>
</div>
<div class="sell-left">
<?php if($page == 1) { ?><?php echo ad($moduleid,$catid,$kw,6);?><?php } ?>
<?php if($page == 1) { ?><?php echo ad($moduleid,0,'',6);?><?php } ?>
          <?php echo tag("moduleid=5&condition=status=3$dtype&areaid=$cityid&catid=$catid&pagesize=".$MOD['pagesize']."&page=$page&showpage=1&datetype=5&order=".$MOD['order']."&fields=".$MOD['fields']."&lazy=$lazy&template=list-sell");?>
</form>
</div>
<div class="sell-right">
<div class="productHot">
            <ul>

<?php $tags=tag("moduleid=5&areaid=$cityid&condition=status=3 and thumb<>''&length=54&pagesize=5&order=addtime desc&template=null");?>

<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
 <li>
                            <div class="productImg"><a href="<?php echo $t['linkurl'];?>" target="_blank"><img src="<?php echo $t['thumb'];?>" alt="<?php echo $t['alt'];?>"></a></div>
                            <div class="productInfo">
                                <div class="productTit"><a href="<?php echo $t['linkurl'];?>" target="_blank"><?php echo $t['title'];?></a></div>
                                <div class="productPrice">价格：
<?php if($t['price']>0) { ?>
￥<?php echo $t['price'];?>/<?php echo $t['unit'];?>
<?php } else { ?>
面议
<?php } ?>
</div>
                                <div class="companyName"><a href="<?php echo userurl($t['username']);?>" target="_blank"><?php echo $t['company'];?></a></div>
                            </div>
                            <i class="hotIcon-2"></i>
                        </li>
<?php } } ?>                      
            </ul>
        </div>
</div>
</div>
<?php include template('footer');?>