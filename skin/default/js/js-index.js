﻿//左侧导航
$('.cate-item').hover(function() {
	$(this).children('.cate-more').show();
}, function() {
	$(this).children('.cate-more').hide();
});

// user service
$('.ub-server a').hover(function(){
            $(this).find('span').animate({top:"-2px"},500)
},function(){
    $(this).find('span').animate({top:'0'},500)
});

// txt scroll
(function ($) {
    $.fn.extend({
        Scroll: function (opt, callback) {
            if (!opt) var opt = {}; var _this = this.eq(0).find("ul:first");
            var lineH = _this.find("li:first").height(), line = opt.line ? parseInt(opt.line, 10) : parseInt(this.height() / lineH, 10), speed = opt.speed ? parseInt(opt.speed, 10) : 500, timer = opt.timer ? parseInt(opt.timer, 10) : 3000;
            if (line == 0) line = 1; var upHeight = 0 - line * lineH;
            _this.hover(function () {
                clearInterval(timerID);
            }, function () { timerID = setInterval(function () { _this.animate({ marginTop: upHeight }, speed, function () { for (i = 1; i <= line; i++) { _this.find("li:first").appendTo(_this); } _this.css({ marginTop: 0 }); }); }, timer); }).mouseout();
        }
    });
})(jQuery);

// txt scroll diaoyong
$(document).ready(function(){
    $(".txts-1").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-2").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-3").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-4").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-5").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-6").Scroll({ line: 1, speed: 800, timer: 4000 });
    $(".txts-7").Scroll({ line: 1, speed: 800, timer: 4000 });

});


// left industry-bar
$(function(){
    var winHeight = $(document).scrollTop();
    $(window).scroll(function() {
        var scrollY = $(document).scrollTop();
        if (scrollY > 100){
            $('.industry-bar').fadeIn();
            $('.reach-bar').addClass('show')
        }
        else {
            $('.industry-bar').fadeOut();
            $('.reach-bar').removeClass('show')
        }
     });
});

// img slide
(function($){
    $.fn.ckSlide = function(opts){
        opts = $.extend({}, $.fn.ckSlide.opts, opts);
        this.each(function(){
            var slidewrap = $(this).find('.ck-slide-wrapper');
            var slide = slidewrap.find('li');
            var count = slide.length;
            var that = this;
            var index = 0;
            var time = null;
            $(this).data('opts', opts);
            $(this).find('.ck-next').on('click', function(){
                if(opts['isAnimate'] == true){
                    return;
                }
                var old = index;
                if(index >= count - 1){
                    index = 0;
                }else{
                    index++;
                }
                change.call(that, index, old);
            });
            $(this).find('.ck-prev').on('click', function(){
                if(opts['isAnimate'] == true){
                    return;
                }
                var old = index;
                if(index <= 0){
                    index = count - 1;
                }else{
                    index--;
                }
                change.call(that, index, old);
            });
            $(this).find('.ck-slidebox li').each(function(cindex){
                $(this).on('click.slidebox', function(){
                    change.call(that, cindex, index);
                    index = cindex;
                });
            });
            $(this).on('mouseover', function(){
                if(opts.autoPlay){
                    clearInterval(time);
                }
                $(this).find('.ctrl-slide').css({opacity:0.6});
            });
            //  leave
            $(this).on('mouseleave', function(){
                if(opts.autoPlay){
                    startAtuoPlay();
                }
                $(this).find('.ctrl-slide').css({opacity:0});
            });
            startAtuoPlay();
            function startAtuoPlay(){
                if(opts.autoPlay){
                    time  = setInterval(function(){
                        var old = index;
                        if(index >= count - 1){
                            index = 0;
                        }else{
                            index++;
                        }
                        change.call(that, index, old);
                    }, 3000);
                }
            }
            var box = $(this).find('.ck-slidebox');
            box.css({
                'margin-left':-(box.width() / 2)
            })
            switch(opts.dir){
                case "x":
                    opts['width'] = $(this).width();
                    slidewrap.css({
                        'width':count * opts['width']
                    });
                    slide.css({
                        'float':'left',
                        'position':'relative'
                    });
                    slidewrap.wrap('<div class="ck-slide-dir"></div>');
                    slide.show();
                    break;
            }
        });
    };
function change(show, hide){
        var opts = $(this).data('opts');
        if(opts.dir == 'x'){
            var x = show * opts['width'];
            $(this).find('.ck-slide-wrapper').stop().animate({'margin-left':-x}, function(){opts['isAnimate'] = false;});
            opts['isAnimate'] = true
        }else{
            $(this).find('.ck-slide-wrapper li').eq(hide).stop().animate({opacity:0,'z-index':0});
            $(this).find('.ck-slide-wrapper li').eq(show).show().css({opacity:0,'z-index':0}).stop().animate({opacity:1,'z-index':10});
        }
        $(this).find('.ck-slidebox li').removeClass('current');
        $(this).find('.ck-slidebox li').eq(show).addClass('current');
    }
    $.fn.ckSlide.opts = {
        autoPlay: false,
        dir: null,
        isAnimate: false
    };
})(jQuery);

// img gundong
$(".bn-img").changeImg({
                    'boxWidth':733,
                    'changeLen':4,
                    'changeSpend':3000,
                    'autoChange':true,
                    'changeHandle':true
                });
$('.focus-img-con').find("li:nth-child(4n+1)").css("border","none");