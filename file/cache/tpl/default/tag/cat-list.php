<?php defined('IN_DESTOON') or exit('Access Denied');?><?php if(is_array($tags)) { foreach($tags as $k => $t) { ?>
<li>    
<div class="p-img"><a target="_blank" href="<?php echo $t['linkurl'];?>"><img src="<?php echo imgurl($t['thumb']);?>" alt="<?php echo $t['alt'];?>"></a></div>
<div class="p-title"><a href="<?php echo $t['linkurl'];?>" target="_blank"><?php echo $t['title'];?></a></div>
<div class="p-price"><?php if($t['unit'] && $t['price']>0) { ?>
<span><em><?php echo $DT['money_sign'];?></em><b><?php echo $t['price'];?></b>/<?php echo $t['unit'];?></span>
<?php } else { ?>
<span>面议</span>
<?php } ?>
</div>
<div class="p-address"><?php echo area_pos($t['areaid'],‘/’, 2);?></div>
<div class="p-shop"><a href="<?php echo userurl($t['username']);?>" target="_blank">商铺</a></div>
</li>
<?php } } ?>