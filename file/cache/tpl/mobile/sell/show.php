<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header');?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>" data-direction="reverse"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
<div class="head-bar-right">
<a href="javascript:Dsheet('<?php if($could_purchase) { ?><a href=&#34;<?php echo $MODULE['2']['mobile'];?>buy.php?mid=<?php echo $moduleid;?>&itemid=<?php echo $itemid;?>&#34;><span>立即购买</span></a>|<?php } ?>
<?php if($could_inquiry) { ?><a href=&#34;<?php echo $MOD['mobile'];?><?php echo rewrite('inquiry.php?itemid='.$itemid);?>&#34; data-transition=&#34;slideup&#34;><span>发送询价</span></a>|<?php } ?>
<a href=&#34;<?php echo DT_MOB;?>api/share.php?mid=<?php echo $moduleid;?>&itemid=<?php echo $itemid;?>&#34; data-transition=&#34;slideup&#34;><span>分享好友</span></a>|<a href=&#34;<?php echo $MOD['mobile'];?>&#34; data-direction=&#34;reverse&#34;><span><?php echo $MOD['name'];?>首页</span></a>|<a href=&#34;<?php echo DT_MOB;?>channel.php&#34; data-direction=&#34;reverse&#34;><span>频道列表</span></a>', '取消');"><img src="<?php echo DT_MOB;?>static/img/icon-action.png" width="24" height="24"/></a>
</div>
</div>
<div class="head-bar-fix"></div>
</div>
<div class="main">
<div class="title"><strong><?php echo $title;?></strong></div>
<div class="info"><?php echo $editdate;?><?php if($MOD['hits']) { ?>&nbsp;&nbsp;浏览:<span id="hits"><?php echo $hits;?></span><?php } ?>
</div>
<?php if($thumb) { ?><?php include template('album', 'chip');?><?php } ?>
<div class="content">
价格:<?php if($price&&$unit) { ?><span class="f_red"><?php echo $DT['money_sign'];?><?php echo $price;?></span>/<?php echo $unit;?><?php } else { ?>未填<?php } ?>
<br/>
<?php if($brand) { ?>品牌:<?php echo $brand;?><br/><?php } ?>
<?php if($n1 && $v1) { ?><?php echo $n1;?>:<?php echo $v1;?><br/><?php } ?>
<?php if($n2 && $v2) { ?><?php echo $n2;?>:<?php echo $v2;?><br/><?php } ?>
<?php if($n3 && $v3) { ?><?php echo $n3;?>:<?php echo $v3;?><br/><?php } ?>
<?php if($minamount) { ?>起订:<?php echo $minamount;?><?php echo $unit;?><br/><?php } ?>
<?php if($amount) { ?>供应:<?php echo $amount;?><?php echo $unit;?><br/><?php } ?>
<?php if($days) { ?>发货:<?php echo $days;?>天内<br/><?php } ?>
<?php if($could_purchase) { ?>
<a href="<?php echo $MODULE['2']['mobile'];?>buy.php?mid=<?php echo $moduleid;?>&itemid=<?php echo $itemid;?>"><div class="btn-green" style="margin:10px 0;">立即购买</div></a>
<?php } else if($could_inquiry) { ?>
<a href="<?php echo $MOD['mobile'];?><?php echo rewrite('inquiry.php?itemid='.$itemid);?>" data-transition="slideup"><div class="btn-blue" style="margin:10px 0;">发送询价</div></a>
<?php } ?>
<?php echo $content;?>
</div>
<div class="head bd-b"><strong>联系方式</strong></div>
<div class="contact"><?php include template('contact', 'chip');?></div>
</div>
<?php include template('comment', 'chip');?>
<?php include template('footer');?>