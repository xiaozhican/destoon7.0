<?php defined('IN_DESTOON') or exit('Access Denied');?><div class="productShowGrid">
<ul>
<?php if(is_array($tags)) { foreach($tags as $k => $t) { ?>
<li id="item_<?php echo $t['itemid'];?>">
                    <div class="productImg"> <a target="_blank" href="<?php echo $t['linkurl'];?>"><img src="<?php echo imgurl($t['thumb']);?>" alt="<?php echo $t['alt'];?>"></a></div>
                    <div class="productInfo">
                        <div class="productTit"><a href="<?php echo $t['linkurl'];?>" target="_blank"><?php echo $t['title'];?></a></div>
                        <div class="companyName">
                            <a href="<?php echo userurl($t['username']);?>" target="_blank"><?php echo $t['company'];?></a>
                            <div class="iconGroup">
                                <i class="iconSty1">诚</i>
                                <i title="" >普通会员</i>
                            </div>
                        </div>
                    </div>
                    <div class="productPrice">
<?php if($t['unit'] && $t['price']>0) { ?>
<span><em><?php echo $DT['money_sign'];?></em><b><?php echo $t['price'];?></b>/<?php echo $t['unit'];?></span>
<?php } else { ?>
<span>面议</span><br/>
<?php } ?>



</div>
<input type="checkbox" id="check_<?php echo $t['itemid'];?>" name="itemid[]" value="<?php echo $t['itemid'];?>" onclick="sell_tip(this, <?php echo $t['itemid'];?>);"/>
                </li>
<?php } } ?>
</ul>
<?php if($showpage && $pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>
</div>