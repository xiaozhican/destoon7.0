//20171127   szy
$(function () {
    // 搜索类型切换
    var searchDiv = $('.searchpop');
    $('#searchType').click(function () {
        $('.searchArrow').toggleClass('cur');
        searchDiv.toggle();
        return false;
    });
    $(document).click(function (e) {
        if (searchDiv.css('display') == 'block') {
            searchDiv.hide();
            $('.searchArrow').removeClass('cur');
        }
    });
    var type = window.location.host;
    if (type.indexOf("company") >= 0)
    {
        $('.seachName').html("企业");
        $('#searchKey').attr({
            'placeholder': '请输入企业名称'
        });
    }
    var typeData = ['产品', '企业', '采购'];
    searchDiv.on('click', 'li', function () {
        var index = $(this).index();
        var type = typeData[index];
        $('.seachName').html(type);
        $('#searchKey').attr({
            'placeholder': '请输入' + type + '名称',
            'data-type': index
        });
        searchDiv.hide();
        $('.searchArrow').removeClass('cur');
    });
	$(document).on("mouseleave", ".searchpop", function () { $(this).hide();$(this).prev().children('.searchArrow').removeClass('cur');});
    //右侧悬浮
    $('.toolbar .tbTab').hover(function () {
        $(this).addClass("tbTabSelected").find('.tabText').stop().animate({ left: "-60px" }, { duration: 300, queue: true });
    },
	function () {
	    $(this).removeClass("tbTabSelected").find('.tabText').stop().animate({ left: "38px" }, { duration: 300, queue: true });
	});
    $('.toolbar .tbTel,.toolbar .tbMoblie').hover(function () {
        $(this).addClass("tbTabSelected").find('.tabText').stop().animate({ left: "-124px" }, { duration: 300, queue: true });
    },
	function () {
	    $(this).removeClass("tbTabSelected").find('.tabText').stop().animate({ left: "38px" }, { duration: 300, queue: true });
	});
    $(".scrollTop").click(function () { $('body,html').animate({ scrollTop: 0 }, 500) });
    
    $(".filterDropDwon li.close").bind("click", function () { $(this).parent(".filterDropDwon").hide(); });
    $(".schForm").mouseleave(function () {
        if ($(this).children(".filterDropDwon").is(":visible")) {
            $(".filterDropDwon").hide();
        }
    });
	//无图onerror
	$('img').each(function(){
        var error = false;
        if (!this.complete) {
            error = true;
        }
        if (typeof this.naturalWidth != "undefined" && this.naturalWidth == 0) {
            error = true;
        }
        if(error){
            $(this).bind('error.replaceSrc',function(){
                this.src = "http://static.qihuiwang.com/images/bgLogoPic.png";

                $(this).unbind('error.replaceSrc');
            }).trigger('load');
        }
    });
    //js延迟加载依赖（方法 $.cachedScript）    
    var scriptsArray = new Array();
    $.cachedScript = function (url, options) { for (var s in scriptsArray) { if (scriptsArray[s] == url) { return { done: function (method) { if (typeof method == "function") { method() } } } } } options = $.extend(options || {}, { dataType: "script", url: url, cache: true }); scriptsArray.push(url); return $.ajax(options) };
    //筛选隐藏显示更多
    $(".showMore").click(function (event) {
        event.preventDefault();
        var ticon = $(this).find("i");
        tspan = $(this).find("span");
        if ($(this).hasClass("zk")) {
            $(this).siblings(".showCon").css("height", "30px");
            ticon.removeClass("icon-shang1");
            ticon.addClass("icon-xia1");
            tspan.html("更多");
            $(this).removeClass("zk")
        } else {
            $(this).siblings(".showCon").css("height", "auto");
            ticon.removeClass("icon-xia1");
            ticon.addClass("icon-shang1");
            tspan.html("隐藏");
            $(this).addClass("zk")
        }
    });
});
