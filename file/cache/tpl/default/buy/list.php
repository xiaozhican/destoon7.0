<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header');?>
<div class="m">
<div class="nav"><a href="<?php echo $MODULE['1']['linkurl'];?>">首页</a> <i>&gt;</i> <a href="<?php echo $MOD['linkurl'];?>"><?php echo $MOD['name'];?></a> <i>&gt;</i> <?php echo cat_pos($CAT, ' <i>&gt;</i> ');?></div>
</div>
<div class="m selectList">
        <dl>
            <dt>行业：</dt>
            <dd> 
      <?php if(is_array($maincat)) { foreach($maincat as $k => $v) { ?>
<a <?php if($v['catid']==$catid) { ?> class="on"<?php } ?>
 href="<?php echo $MOD['linkurl'];?><?php echo $v['linkurl'];?>"><?php echo set_style($v['catname'],$v['style']);?></a>
<?php } } ?>
            </dd>
        
        </dl>
        <dl>
            <dt>地区：</dt>
            <dd> 
        <?php $mainarea = get_mainarea(0)?>
<?php if(is_array($mainarea)) { foreach($mainarea as $k => $v) { ?>
<a href="<?php echo $MOD['linkurl'];?><?php echo rewrite('search.php?catid='.$catid.'&areaid='.$v['areaid']);?>"><?php echo $v['areaname'];?></a>
<?php } } ?>
            </dd>
        </dl>
 <dl>
            <dt>类别：</dt>
            <dd> 
       <?php if(is_array($TYPE)) { foreach($TYPE as $k => $v) { ?>
<a href="<?php echo $MOD['linkurl'];?><?php echo rewrite('search.php?catid='.$catid.'&typeid='.$k);?>"><?php echo $v;?></a>
<?php } } ?>
            </dd>
        </dl>
    </div>
<div class="w1200">
<div class="cg-left">

<?php if($CP) { ?><?php include template('property_list', 'chip');?><?php } ?>
<div class="img_tip" id="img_tip" style="display:none;">&nbsp;</div>
<div class="cg-list">
<ul>
<?php if($page == 1) { ?><?php echo ad($moduleid,$catid,$kw,6);?><?php } ?>
<?php if($tags) { ?><?php include template('list-'.$module, 'tag');?><?php } ?>
</ul>
</div>
</div>
<div class="cg-right">
<div class="head-sub"><strong>搜索排行</strong></div>
<div class="list-rank">
<?php echo tag("moduleid=6&table=keyword&condition=moduleid=$moduleid and status=3 and updatetime>$today_endtime-86400*30&pagesize=9&order=week_search desc&key=week_search&template=list-search_rank");?>
</div>
</div>
<div class="c_b"></div>
</div>
<?php include template('footer');?>