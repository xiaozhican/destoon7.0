<?php defined('IN_DESTOON') or exit('Access Denied');?><ul>
<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
 <?php if($t['thumb']) { ?><li>
<a target="blank" href="<?php echo $t['linkurl'];?>"><img src="<?php echo imgurl($t['thumb']);?>" alt="<?php echo $t['alt'];?>"/></a>
<div class="text">
<h5><a target="blank" href="<?php echo $t['linkurl'];?>"><?php echo $t['title'];?></a></h5>
<p><?php echo dsubstr($t['introduce'], 250, '…');?></p>
<i>标签:
<?php $keytags = $t[tag] ? explode(' ', $t[tag]) : array();?>
<?php if(is_array($keytags)) { foreach($keytags as $h) { ?>
<a href="<?php echo $MODULE['moduleid']['linkurl'];?>search.php?kw=<?php echo urlencode($h);?>" target="_blank" rel="nofollow"><?php echo $h;?></a>
<?php } } ?>
</i>
<em><?php echo date("Y-m-d ",$t['addtime']);?></em>
 </div>                                  
</li>
<?php } else { ?>
<li class="nopic">
<div class="text">
<h5><a target="blank" href="<?php echo $t['linkurl'];?>"><?php echo $t['title'];?></a></h5>
<p><?php echo dsubstr($t['introduce'], 250, '…');?></p>
<i>标签:
<?php $keytags = $t[tag] ? explode(' ', $t[tag]) : array();?>
<?php if(is_array($keytags)) { foreach($keytags as $h) { ?>
<a href="<?php echo $MODULE['moduleid']['linkurl'];?>search.php?kw=<?php echo urlencode($h);?>" target="_blank" rel="nofollow"><?php echo $h;?></a>
<?php } } ?>
</i>
<em><?php echo date("Y-m-d ",$t['addtime']);?></em>
 </div>                                    
</li>
<?php } ?>
<?php } } ?>
</ul>
<div class="b10"></div>
<?php if($showpage && $pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>