<?php
/**
 * 联盟网站的数据接收接口。
 * company 会员与商铺的同步创建; product 供应信息的创建
 *
 * author: wzgwzg@qq.com
 * date: 2018年6月5日
 */
if($_POST['type'] == 'product') {
	$module = 'sell';
	$moduleid = 5;
} else if($_POST['type'] == 'company') {
	$module = 'company';
	$moduleid = 4;
}

require '../common.inc.php';
if($_POST['key'] != md5(mktime(10,0,0) . 'com.zh30.www')) {
	msg(0,'key error.');
}
if($_POST['type'] == 'company') {
	//推广企业
	$data = json_decode(stripcslashes($data), true);

	//检查用户是否已经存在
	$member_exists = $db->get_one("SELECT * FROM {$DT_PRE}member WHERE username='{$data['m']['username']}'");
	if($member_exists) {
		msg(0,'会员已经存在.');
	}
	//构造member数据sql
	$msql_k = $msql_v = '';
	foreach($data['m'] as $mk=>$mv) {
		if($mk == 'userid') continue;
		$msql_k .= '`'.$mk.'`,';
		$msql_v .= '\''.$mv.'\',';
	}
	$msql_k = substr($msql_k, 0, -1);
	$msql_v = substr($msql_v, 0, -1);

	$db->query("INSERT INTO {$DT_PRE}member ({$msql_k}) VALUES ({$msql_v})");
	$userid = $db->insert_id();
	if(!$userid) {
		msg(0,'推送会员数据失败.');
	}

	//构造company数据
	$data['c']['linkurl'] = userurl($data['m']['username']);	//解决推送的店铺链接是链到主站的问题。
	$csql_k = $csql_v = $c_content = '';
	foreach($data['c'] as $ck=>$cv) {
		if($ck == 'content') {
			$c_content = $cv;
			continue;
		}

		$csql_k .= '`'.$ck.'`,';
		if($ck == 'userid') {
			$csql_v .= '\''.$userid.'\',';
			continue;
		}
		$csql_v .= '\''.$cv.'\',';
	}
	$csql_k = substr($csql_k, 0, -1);
	$csql_v = substr($csql_v, 0, -1);
	$c = $db->query("INSERT INTO {$DT_PRE}company ({$csql_k}) VALUES ({$csql_v})");
	$cc= $db->query("INSERT INTO {$DT_PRE}company_data (userid, content) VALUES ('$userid', '$c_content')");
	if($c && $cc) {
		msg(1,'推送成功.');
	} else {
		msg(0,'推送公司数据失败.');
	}
} else if($_POST['type'] == 'product') {
	
	require DT_ROOT.'/module/'.$module.'/common.inc.php';
	require DT_ROOT.'/include/post.func.php';
	require MD_ROOT.'/sell.class.php';
	$data = json_decode(stripcslashes($_POST['data']), true);
	$post = $data['product'];
	//sell
	$do = new sell(5);
	//之前已经发布过的商品 直接刷新
	if($web_sell_id) {
		$product = $db->get_one("SELECT itemid FROM {$DT_PRE}sell_5 WHERE itemid='{$web_sell_id}'");
		if($product) {
			$do->refresh($product['itemid']);
			msg(1,'刷新成功.', array('sell_id'=>$product['itemid']));
			exit;
		}
	}

	$post = $do->set($post);
	$itemid = $do->add($post);
	if(!$itemid) {
		msg(0, '推送失败');
	}
	msg(1,'推送成功.', array('sell_id'=>$itemid));
} else {
	msg(0,'参数错误.');
}

function msg($status, $msgstr='', $addon=array()) {
	$msg = array();
	$msg['status'] = $status;
	$msg['msg'] = $msgstr;
	$msg = array_merge($msg, $addon);  
	exit(json_encode($msg));
}
?>