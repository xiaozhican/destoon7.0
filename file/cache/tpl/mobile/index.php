<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header');?>
<?php if($ads) { ?>
<div id="mobile-slide" class="slide">
<?php if(is_array($ads)) { foreach($ads as $t) { ?>
<a href="<?php echo $t['url'];?>" target="_blank" rel="external"><img src="<?php echo $t['image_src'];?>" alt="<?php echo $t['image_alt'];?>" style="width:100%;"/></a>
<?php } } ?>
</div>
<?php } ?>
<div class="nav-main">
<ul>
<li>
<a href="<?php echo $MODULE['5']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m1.png">
<span class="text">供应</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['6']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m2.png">
<span class="text">求购</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['7']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m3.png">
<span class="text">新闻</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['8']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m4.png">
<span class="text">企业</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['7']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m5.png">
<span class="text">招商</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['7']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m6.png">
<span class="text">行情</span>
</a>
</li>

<li>
<a href="<?php echo $MODULE['7']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m7.png">
<span class="text">展会</span>
</a>
</li>
<li>
<a href="<?php echo $MODULE['7']['mobile'];?>">
<img src="<?php echo DT_MOB;?>static/img/m8.png">
<span class="text">问答</span>
</a>
</li>
</ul>
</div>
<div class="b10"></div>
<div class="product-box">
<h2><em></em>产品推荐<em></em></h2>
<ul>
<?php $tags=tag("moduleid=5&areaid=$cityid&condition=status=3 and thumb<>''&length=54&pagesize=10&order=addtime desc&template=null");?>
<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
<li><a href="<?php echo str_replace("//www.","//m.",$t['linkurl']);?>"><img src="<?php echo imgurl($t['thumb'], 1);?>" alt="<?php echo $t['title'];?>">
<p><?php echo $t['title'];?></p>
<?php if($t['unit'] && $t['price']>0) { ?>
<p class="price"><?php echo $DT['money_sign'];?><?php echo $t['price'];?>/<?php echo $t['unit'];?></p>
<?php } else { ?>
<p class="price">面议</p>
<?php } ?>
</a>
<?php if($t['vip']) { ?><div class="c1"><i>实名认证</i></div><?php } ?>
</li><?php } } ?>
</ul>
<a class="more" href="<?php echo $MODULE['5']['mobile'];?>">更多产品</a>
</div>
<div class="product-box">
<h2><em></em>牛企推荐<em></em></h2>
</div>
<div class="s-list">
<ul>
<?php $tags=tag("moduleid=4&condition=groupid>5 and thumb<>'' and catids<>''&pagesize=5&order=vip desc&template=null");?>
<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
<li><a href="<?php echo $t['linkurl'];?>"><img src="<?php echo imgurl($t['thumb'], 1);?>" alt="<?php echo $t['title'];?>">
<div class="text">
<h2<?php if($t['vip']) { ?> class="vip" title="<?php echo VIP;?>:<?php echo $t['vip'];?>"<?php } ?>
><?php echo $t['title'];?></h2>
<p class="c0"><?php echo area_pos($t['areaid'], '');?></p>
<p class="c1"><i>实地认证</i><i>买家保障</i></p>
<p class="c3">主营:<?php echo $t['business'];?></p>
</div></a>
</li><?php } } ?>
</ul>
</div>
<a class="more" href="/company/">更多商家</a>
<div id="wm0" class="colum">
<div class="qc-head">
<li class="current">最新资讯</li>
<li>市场报价</li>
</div>
<div class="qc-body">
<dl>
<div class="news-list">
<ul>
<?php $tags=tag("moduleid=21&areaid=$cityid&condition=status=3 and thumb<>''&length=54&pagesize=10&order=addtime desc&template=null");?>
<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
<li><a href="<?php echo str_replace("//www.","//m.",$t['linkurl']);?>"><img src="<?php echo imgurl($t['thumb'], 1);?>" alt="<?php echo $t['title'];?>"><div class="text"><p><?php echo $t['title'];?></p><em><?php echo timetodate($t['addtime'],3);?> </em></div></a>
</li><?php } } ?>
</ul>
  <a class="more" href="news/">更多新闻</a>
</div>
</dl>
<dl>
<div class="news-list">
<ul class="only">
<?php $tags=tag("moduleid=7&areaid=$cityid&condition=status=3&length=54&pagesize=20&order=addtime desc&template=null");?>
<?php if(is_array($tags)) { foreach($tags as $i => $t) { ?>
<li><i><?php echo timetodate($t['addtime'],3);?> </i><a href="<?php echo str_replace("//www.","//m.",$t['linkurl']);?>"><?php echo $t['title'];?></a></li><?php } } ?>
</ul>
  <a class="more" href="quote/">更多行情</a>
</div>
</dl>
</div>
</div>
<script language="javascript">qc_register('wm0', 2);</script>
<script type="text/javascript">
var C = <?php echo $MOD_MY['0']['moduleid'];?>;
var P = 1;
function Dmenu(i) {
if(i == C) return;
$('.spin-more').show();
$('.spin-load').show();
$('.home-goto').hide();
$('#m_'+C).removeClass('on');
C = i;
$('#m_'+C).addClass('on');
P = 1;
$('html, body').animate({scrollTop:$('#segment').offset().top-<?php if($DT_MOB['browser']=='b2b'&&$DT_MOB['os']=='ios') { ?>108<?php } else { ?>88<?php } ?>
}, 500);
$('#channel').addClass('list-menu-fix');
Dload();
}
function Dstop() {
$('.spin-more').hide();
$('.spin-load').hide();
$('.home-goto a').attr('href', $('#m_'+C).attr('data-href'));
$('.home-goto div').html('进入'+$('#m_'+C).html()+'频道');
$('.home-goto').show('slow');
}
function Dload() {
if(P > 3) {
Dstop();
return;
}
$.get($('#m_'+C).attr('data-href')+'search.php?action=ajax&page='+P, function(result){
$('.spin-load').hide();
if(result && result.indexOf('list-empty') == -1) {
P == 1 ?  $('#main').html(result) : $('#main').append(result);
} else {
if(P == 1) $('#main').html('<br/><br/><center>暂无信息</center><br/><br/>');
P = 4;
Dstop();
}
});
}
<?php if($ads) { ?>
function dslide(id, time) {
if($('#'+id).html().indexOf('<ul') != -1) return;
var _this = this;
this.w = $(document).width();
this.h = 1;//$('#'+id).find('img')[0].height();
this.c = 0;
this.src = [];
this.url = [];
this.alt = [];
this.tar = [];
$('#'+id).find('a').each(function(i) {
_this.src.push($(this).find('img')[0].src);
_this.alt.push($(this).find('img')[0].alt);
_this.url.push(this.href);
_this.tar.push(this.target);
});
if(!this.src[0]) return;
this.max = this.src.length;
this.htm = '<ul id="'+id+'_ul" style="position:relative;width:'+this.w*(this.max+1)+'px;z-index:1;overflow:hidden;">';//height:'+this.h+'px;
for(var i = 0; i < this.max; i++) {
this.htm += '<li style="float:left;"><a href="'+this.url[i]+'" target="'+this.tar[i]+'"><img src="'+this.src[i]+'" width="'+this.w+'"/></a></li>';
}
this.htm += '</ul>';
if(this.alt[0]) this.htm += '<div id="'+id+'_alt" style="width:'+(this.w-32)+'px;height:32px;line-height:32px;overflow:hidden;z-index:3;position:absolute;margin-top:-32px;padding:0 16px;font-weight:bold;color:#FFFFFF;background:#384349;filter:Alpha(Opacity=80);opacity:0.8;">'+this.alt[0]+'</div>';
this.htm += '<div style="width:'+this.w+'px;height:20px;overflow:hidden;z-index:4;position:absolute;margin-top:-'+(this.alt[0] ? 60 : 30)+'px;text-align:center;padding-left:6px;">';
for(var i = 0; i < this.max; i++) {
this.htm += '<span id="'+id+'_no_'+i+'" style="display:inline-block;width:6px;height:6px;border:1px solid #FFFFFF;border-radius:6px;margin-right:6px;'+(i == this.c ? 'background:#007AFF;' : 'background:#FFFFFF;')+'"></span>';
}
this.htm += '</div>';
$('#'+id).html(this.htm);
if(this.max == 1) return;
this.t;
this.p = 0;
$('#'+id).mouseover(function() {_this.p=1;});
$('#'+id).mouseout(function() {_this.p=0;});
$('#'+id).find('span').each(function(i) {
$(this).mouseover(function() {
_this.slide(i);
});
});
this.slide = function(o) {
if(o == this.c) return;
if(o < 0 || o >= this.max) return;
if(o == 0 && this.c == this.max - 1) {
$('#'+id+'_ul').append($('#'+id+'_ul li:first').clone());
$('#'+id+'_ul').animate({'left':-this.w*this.max},500,function() {
$('#'+id+'_ul').css('left','0');
$('#'+id+'_ul li:last').remove();
});
} else {
$('#'+id+'_ul').animate({'left':-o*this.w},500);
}
$('#'+id+'_no_'+this.c).css('background','#FFFFFF');
$('#'+id+'_no_'+o).css('background','#007AFF');
if(this.alt[0]) $('#'+id+'_alt').html(this.alt[o]);
this.c = o;
}
this.start = function() {
if(this.p) return;
if(this.c == this.max - 1) {
this.slide(0);
} else {
this.slide(this.c+1);
}
}
if(!time) time = 5000;
this.t = setInterval(function() {_this.start();}, time);
return true;
}
<?php } ?>
$(document).on('pageinit', function(event) {
//$(document).ready(function() {
$('#channel li').each(function(i){
$(this).html($(this).text());
});
$('.spin-more').show();
Dload();
$('#main').on('scrollstop',function(event){
var cmh2 = $(window).height();
var cmh1 = $('.spin-more').offset().top;
if($(document).scrollTop() + cmh2 >= cmh1-100) {
P++;
Dload();
}
//if($(document).scrollTop() > T-44) {
if($(document).scrollTop() > $('#segment').offset().top) {
$('#channel').addClass('list-menu-fix');
} else {
$('#channel').removeClass('list-menu-fix');
}
});
<?php if($ads) { ?>
var ds = new dslide('mobile-slide');
if(ds) {
$('#mobile-slide').on('swipeleft',function(){
ds.slide(ds.c+1);
});
$('#mobile-slide').on('swiperight',function(){
ds.slide(ds.c-1);
});
$(window).bind('orientationchange.slide', function(e){
ds.w = $(document).width();
$('#mobile-slide').find('ul').css('width', ds.w*(ds.max+1));
$('#mobile-slide').find('img').css('width', ds.w);
$('#mobile-slide').find('div').css('width', ds.w);
});
}
<?php } ?>
});
</script>
<?php include template('footer');?>