<?php defined('IN_DESTOON') or exit('Access Denied');?><?php include template('header', $template);?>
<div id="head-bar">
<div class="head-bar">
<div class="head-bar-back">
<a href="<?php echo $back_link;?>" data-direction="reverse"><img src="<?php echo DT_MOB;?>static/img/icon-back.png" width="24" height="24"/><span>返回</span></a>
</div>
<div class="head-bar-title"><?php echo $head_name;?></div>
</div>
<div class="head-bar-fix"></div>
</div>
<?php if($itemid) { ?>
<div class="main">
<div class="title"><strong><?php echo $title;?></strong></div>
<div class="info"><?php echo timetodate($addtime, 3);?>&nbsp;&nbsp;点击:<?php echo $hits;?></div>
<div class="content">
<img src="<?php echo $image;?>" alt="<?php echo $title;?>"/><br/>
发证机构：<?php echo $authority;?><br/>
发证时间 ：<?php echo timetodate($fromtime, 3);?><br/>
有效期至 ：<?php if($totime) { ?><?php echo timetodate($totime, 3);?><?php } else { ?>永久<?php } ?>
<br/>
<?php echo $content;?>
</div>
</div>
<?php } else { ?>
<?php if(is_array($lists)) { foreach($lists as $v) { ?>
<div class="list-img">
<a href="<?php echo $v['linkurl'];?>"><img src="<?php echo $v['thumb'];?>" width="60" height="60" alt="" onerror="this.src='<?php echo DT_MOB;?>static/img/60x60.png';"/></a>
<ul>
<li><a href="<?php echo $v['linkurl'];?>"><strong><?php echo $v['title'];?></strong></a></li>
<li><span class="f_r"><?php echo timetodate($v['fromtime'], 3);?> 至 <?php if($v['totime']) { ?><?php echo timetodate($v['totime'], 3);?><?php } else { ?>永久<?php } ?>
</span><span><?php echo $v['authority'];?></span></li>
</ul>
</div>
<?php } } ?>
<?php if($pages) { ?><div class="pages"><?php echo $pages;?></div><?php } ?>
<?php } ?>
<?php include template('footer', $template);?>