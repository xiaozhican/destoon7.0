<?php 
defined('IN_DESTOON') or exit('Access Denied');
login();

require DT_ROOT.'/module/'.$module.'/common.inc.php';
require MD_ROOT.'/member.class.php';
require DT_ROOT.'/include/post.func.php';

$do = new member;
$do->userid = $_userid;
$user = $do->get_one();
if($user['groupid'] < 7 ) dalert('您还不是VIP用户，请升级!', "/member/grade.php");	//alert提示框 或 message()方法使用destoon系统提示框

if($action == 'history') {
	//status 为1 是显示 0 是不显示的。
	$webs = $db->query("SELECT id,web,name FROM {$DT_PRE}webs where status=1 order by id desc");

	$sendwebs = array();
	
	while($web = $db->fetch_array($webs)){
		
		$sendwebs[$web['id']] = $web;
		
	}

	$r = $db->get_one("SELECT COUNT(*) AS num FROM {$DT_PRE}webs_flow a LEFT JOIN {$DT_PRE}webs b ON a.web_id=b.id where  a.userid=$_userid and type=1");
	$pages = pages($r['num'], $page, 20);
	$products = $db->query("SELECT * FROM {$DT_PRE}webs_flow a LEFT JOIN {$DT_PRE}webs b ON a.web_id=b.id where a.userid=$_userid order by add_time desc LIMIT $offset,$pagesize");
	while($product = $db->fetch_array($products)) {
		if($product['type'] == 0){
			$company = $db->get_one("SELECT company FROM {$DT_PRE}company where username = '$_username'");
			$product['title'] = $company['company'];
			$product['url'] = $product['web'] . '/com/'.$_username;
		}else{
			$sell = $db->get_one("SELECT title FROM {$DT_PRE}sell_5 where itemid={$product['sell_id']} and username = '$_username'");
			$product['title'] = $sell['title'];
			$product['url'] = $product['web'] . '/sell/show-' . $product['web_sell_id'] . '.html';
		}
		$data[] = $product;
	}
}else{
	$send_company = $db->get_one("SELECT * FROM {$DT_PRE}webs_flow where userid=$_userid and type=0");
	$send_products = $db->query("SELECT * FROM {$DT_PRE}sell_5 where username= '$_username' order by addtime desc limit 0,50");
	 while($v = $db->fetch_array($send_products)) {
	 		$v['price'] = $v['price'] != 0?$v['price']:'面议';
			$data[] = $v;
	 }
	//所有的网站。
	$webs = $db->query("SELECT id,web,name FROM {$DT_PRE}webs where status=1 order by id desc");
	$sendwebs = array();
	
	$json_webs='[';
	$i=0;
	while($web = $db->fetch_array($webs)){
		$sendwebs[]=$web;
		if($i>0) {
			$json_webs.=',';
		}
		$json_webs.='{"id":"'.$web['id'].'","web":"'.$web['web'].'","name":"'.$web['name'].'"}';
		$i++;
	}
	$json_webs.=']';
	//echo $json_webs;
	//$json_webs = json_encode($sendwebs);
}

include template('sp_product', $module);
?>
